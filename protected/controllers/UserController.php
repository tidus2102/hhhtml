<?php

class UserController extends Controller {
    
    public $layout = '//layouts/user';
    
    /**
     * @var string specifies the default action
     */

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',  // all users
                'actions'=>array(
                    'index', 'profile', 'community'
                    ),
                'users'=>array('*'),
            ),
//            array('allow', # logged in users
//                    'actions'=>array('update', 'welcome', 'activationNeeded', 'changePassword'),
//                    'users'=>array('@'),
//                ),
//            array('allow', # admins
//                'actions'=>array('admin'),
//                'roles'=>array('admin'),
//                'users'=>array('@'),
//            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Shows a particular user.
     */
    public function actionIndex() {
        $this->render('index');
    }
    
    public function actionProfile() {
        $this->render('profile');
    }
    
    public function actionCommunity() {
        $this->render('community');
    }
}


