<?php

class DoctorController extends Controller {
    
    public $layout = '//layouts/doctor';
    
    /**
 	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			# captcha action renders the CAPTCHA image displayed on the contact page
			#'captcha'=>array(
			#	'class'=>'CCaptchaAction',
			#	'backColor'=>0xFFFFFF,
			#),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',  // all users
                  'actions'=>array(
                      'index', 
//                      'warning', 
                      'patient', 'community', 'account', 
                      'profile', 'community2',
                  ),
                  'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
//		$this->redirect('warning');
	}
    
    public function actionProfile() {
		$this->render('profile');
	}
    
    public function actionCommunity2() {
		$this->render('community2');
	}
    
//    public function actionWarning() {
//		$this->render('warning');
//	}
    
    public function actionPatient() {
		$this->render('patient');
	}
    
    public function actionCommunity() {
		$this->render('community');
	}
    
    public function actionAccount() {
		$this->render('account');
	}
}
