<?php

class PatientController extends Controller {
    
    public $layout = '//layouts/patient';
    
    /**
 	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			# captcha action renders the CAPTCHA image displayed on the contact page
			#'captcha'=>array(
			#	'class'=>'CCaptchaAction',
			#	'backColor'=>0xFFFFFF,
			#),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',  // all users
                  'actions'=>array(
                      'index', 'health', 
                      //'discussion', 
                      'doctor', 'guardian', 
                      'community', 'account', 'notification'
                  ),
                  'users'=>array('*'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionNotification() {
        $this->render('notification');
    }
        
    public function actionAccount() {
        $this->render('account');
    }
    
    public function actionIndex() {
		$this->render('index');
	}
    
    public function actionHealth() {
		$this->render('health');
	}
    
//    public function actionDiscussion() {
//		$this->render('discussion');
//	}
    
    public function actionDoctor() {
		$this->render('doctor');
	}
    
    public function actionGuardian() {
		$this->render('guardian');
	}
    
    public function actionCommunity() {
		$this->render('community');
	}
}
