<?php

class File extends FileHaver
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function tableName()
	{
		return 'file';
	}
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>64),
			array('folder', 'length', 'max'=>64),
			array('created_at', 'safe'),
		);
	}
	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'folder' => 'Folder',
			'created_at' => 'Created At',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function saveFile($type){
        $this->updateFile($type);
    }
}
