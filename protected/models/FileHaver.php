<?php

class FileHaver extends CActiveRecord {
    public function createDirs($type) {
        $dir = Yii::getPathOfAlias('filePath');
        Yii::log(__FUNCTION__ . ">> Directory:" . $dir, 'debug');
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        if (!file_exists($dir)) mkdir($dir);
        $dir .= "/$type";
        if (!file_exists($dir)) mkdir($dir);
        $dir .= "/$year";
        if (!file_exists($dir)) mkdir($dir);
        $dir .= "/$month";
        if (!file_exists($dir)) mkdir($dir);
        $dir .= "/$day";
        if (!file_exists($dir)) mkdir($dir);

        $this->folder = "$type/$year/$month/$day";
        return $this->folder;
    }

    public function getPath() {
        if(!isset($this->id)) return '';
        $fileName = $this->name;
        $folder = $this->folder;
        $dir = Yii::getPathOfAlias('filePath') ;
        $path = $dir . "/$folder/$fileName";
        if (file_exists($path)) return $path;
        return '';
    }

    public function getUrl() {
        $fileName = $this->name;
        $folder = $this->folder;
        $dir = Yii::getPathOfAlias('fileURL');
        $url = $dir . "/$folder/$fileName";
        return $url;
    }

    public function removeFile(){
        $file_path = $this->getPath();
        if(unlink($file_path)) return true;
        return false;
    }

    public function chooseFile($type, $htmlOptions=array()) {
        $file = $this->getPath();
        if ($file !== null && file_exists($file)) {
            Yii::log(__FUNCTION__ . '>> ' . $file, 'debug');
            echo CHtml::openTag('div', $htmlOptions);
            echo CHtml::radioButton("use_$type", true, array('value'=>'current')) .
                Yii::t('app', ' use ') . CHtml::link(Yii::t('app', "current"), $this->getUrl(), array('target'=>'_blank'));
            echo ' ';
            echo CHtml::radioButton("use_$type", false, array('value'=>'')) . ' ' . CHtml::fileField($type);
            echo CHtml::closeTag('div');
        } else {
            echo CHtml::fileField($type);
        }
    }

    public function updateFile($type){
        if(($instance = self::getUploadedFileInstance($type)) !== null){
            if(!$this->isNewRecord){//create new
                $this->removeFile();
            }
            $fileName = md5(Util::randomizeString(20)) . time() . '.' . $instance->extensionName;
            $this->name = $fileName;
            $folder = $this->createDirs($type);
            $file_path = Yii::getPathOfAlias('filePath') . "/$folder/$fileName";
            Yii::log(__FUNCTION__ . ">> " . $file_path, 'debug');
            if($instance->saveAs($file_path)){
                if($this->save(false)) return true;
            }
        }
        return false;
    }

    public static function getUploadedFileInstance($type){
        if(!isset($_POST["use_$type"]) || $_POST["use_$type"]!= 'current'){
            $file = CUploadedFile::getInstanceByName($type);
            return ($file!==null)? $file : null;
        }
        return null;
    }
}
