<div class="info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/patient.jpg"></a>
            <a href=""><h3>Nguyễn Tuấn Huy</h3></a>
            <ul class="nav nav-info">
                <li>Male, 60</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 infor">
            <div class="infoc">
                <a href=""><h3>Dr Trần Công Minh</h3></a>
                <ul class="nav nav-info">
                    <li>Male, 50</li>
                    <li>Hồ Chí Minh</li>
                </ul>
            </div>
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
        </div>
    </div>
</div>
<div class="tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/patient/">Health</a></li>
            <li><a href="/patient/doctor">Doctor & Guardian</a></li>
            <!--<li><a href="/patient/guardian">Guardian</a></li>-->
            <!--<li><a href="/patient/community">Community</a></li>-->
<!--            
            <li class="active"><a href="/patient/health"><i class="fa fa-heart fa-lg"></i> Health</a></li>
            <li><a href="/patient/discussion"><i class="fa fa-comments fa-lg"></i> Discussion</a></li>
            <li><a href="/patient/doctor"><i class="fa fa-user-md fa-lg"></i> Doctor</a></li>
            <li><a href="/patient/guardian"><i class="fa fa-user fa-lg"></i> Guardian</a></li>
            <li><a href="/patient/community"><i class="fa fa-group fa-lg"></i> Community</a></li>-->
        </ul>
    </div>
</div>
<div class="health-block">
    <div class="container">
        
        <!--<div class="clearfix"></div>-->
        <div class="panel panel-default margin-t">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bar-chart-o fa-lg"></i>
                </span>
                Blood Pressure Record
            </div>
            <div class="panel-body">
                <div class="">
                    <div class="pull-left">
                        <ul data-tabs="tabs" class="nav nav-pills ">
                            <li class="active"><a href="#f1" data-toggle="tab">All</a></li>
                            <li><a href="#f1" data-toggle="tab">Morning</a></li>
                            <li><a href="#f2" data-toggle="tab">Noon</a></li>
                            <li><a href="#f3" data-toggle="tab">Afternoon</a></li>
                            <li><a href="#f4" data-toggle="tab">Evening</a></li>
                        </ul>
                    </div>
                    <div class="pull-right">
                        <ul class="nav nav-pills ">
                            <li class="active"><a href="#">3 days</a></li>
                            <li><a href="#">1 week</a></li>
                            <li><a href="#">2 weeks</a></li>
                            <li><a href="#">1 month</a></li>
                            <li><a href="#">3 months</a></li>
                            <li><a href="#">6 months</a></li>
                        </ul>
                        <form role="form" class="form-horizontal">
                            <div class="control-group">
                                <label class="col-sm-2 control-label">From</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="col-sm-2 control-label">To</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content content">
                    <div id="f1" class="tab-pane active">
                        <div class="col-sm-12 margin-b">
                            <img src="/img/sample/linechart2.jpg" >
                        </div>
                        <div class="col-sm-5">
                            <?/*<div class="panel mini-box">
                                <p class="size-h2 margin-b text-center">Doctor's Note</p>
                                <form role="form" class="form-horizontal margin-b">
                                    <textarea rows="3" class="form-control" placeholder="Add note"></textarea>
                                </form>
                                <ul class="nav note-list">
                                    <li>
                                        <i class="fa fa-check-square-o"></i>
                    <!--                    <strong>Rod</strong> uploaded 6 files. -->
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                        <br>
                                        <small class="text-muted">
                                            about 4 hours ago
                                            <a href="" class="pull-right">Delete</a>
                                        </small>
                                    </li>
                                    <li>
                                        <i class="fa fa-check-square-o"></i>
                    <!--                    <strong>Rod</strong> uploaded 6 files. -->
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                        <br>
                                        <small class="text-muted">
                                            about 4 hours ago
                                            <a href="" class="pull-right">Delete</a>
                                        </small>
                                    </li>
                                    <li>
                                        <i class="fa fa-check-square-o"></i>
                    <!--                    <strong>Rod</strong> uploaded 6 files. -->
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                        <br>
                                        <small class="text-muted">
                                            about 4 hours ago
                                            <a href="" class="pull-right">Delete</a>
                                        </small>
                                    </li>
                                </ul>
                            </div>*/?>
                            <div class="record-list perfectScrollbar margin-t">
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Sat, 25-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
<!--                                        <div class="round-icon bg-info">
                                            <span class="point1">201</span>
                                            <span class="point2">111</span>
                                            <span class="point3">75</span>
                                        </div>-->

<!--                                        <span class="round-icon bg-primary point1">201</span>
                                        <span class="round-icon bg-primary point2">111</span>
                                        <span class="round-icon bg-primary point3">76</span>-->
                                    </div>
                                    <div class="feed-subject">
                                        <!--<p>-->
<!--                                            <span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span>-->
                                            <!--<div class="btn-group">-->
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <!--</div>-->
                                            <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Morning</span>-->
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                        <!--</p>-->
                                    </div>
<!--                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>-->
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Afternoon</span>-->
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href=""><span class="round-icon bg-success"><i class="fa fa-smile-o"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">150</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">90</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">70</a>
                                        <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Evening</span>-->
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 0</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 19:00"><i class="fa fa-clock-o"></i> 19:00</span>
                                    </div>
                                </div>
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Fri, 24-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 24-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 24-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Thu, 23-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href=""><span class="round-icon bg-success"><i class="fa fa-smile-o"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">150</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">90</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">70</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 0</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 19:00"><i class="fa fa-clock-o"></i> 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-7">
                            <?/*<div class="action-box">
                                <textarea placeholder="Create your topic..." rows="3" class="form-control"></textarea>
                                <div class="action-list">
                                    <div class="action-item">
                                        <a title="" class="fa fa-picture-o" rel="tooltip" data-original-title="Post an Image"><i></i></a>
                                        <a title="" class="fa fa-video-camera" rel="tooltip" data-original-title="Upload a Video"><i></i></a>
                    <!--                    <a title="" class="fa fa-lightbulb-o" rel="tooltip" data-original-title="Post an Idea"><i></i></a>
                                        <a title="" class="fa fa-question-circle" rel="tooltip" data-original-title="Ask a Question"><i></i></a>-->
                                    </div>	
                                    <div class="pull-right">
                                        <a class="btn btn-primary btn-sm">Post</a>
                                    </div>
                                </div>
                            </div>*/?>
                            <textarea placeholder="Create your topic..." rows="3" class="form-control"></textarea>
                            <div class="margin-t">
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <!--<i class="fa fa-lightbulb-o"></i>-->
                                        <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Dr Trần Công Minh</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                    <div class="feed-actions">
                        <!--                <a class="pull-left" rel="tooltip"><i class="fa fa-thumbs-up"></i> 123</a> 
                                        <a class="pull-left" rel="tooltip"><i class="fa fa-comment-o"></i> 29</a>-->
                                        <a class="pull-right" href="#">Reply</a>
                                    </div>
                                    <div class="feed-item">
                                        <div class="feed-icon">
                                            <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                                        </div>
                                        <div class="feed-subject">
                                            <p>
                                                <a href="#">Nguyễn Tuấn Huy</a>
                                                <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                            </p>
                                        </div>
                                        <div class="feed-content">
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="f2" class="tab-pane">
                        Morning
                    </div>
                    <div id="f3" class="tab-pane">
                        Afternoon
                    </div>
                    <div id="f4" class="tab-pane">
                        Evening
                    </div>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bar-chart-o fa-lg"></i>
                </span>
                Heart Rate Record
            </div>
            <div class="panel-body">
                <img src="/img/sample/linechart2.jpg" >
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Summary
            </div>
            <div class="panel-body">
                <div class="col-sm-3">
                    <div class="panel mini-box">
                        <span class="box-icon bg-primary">
                            <i class="fa fa-database"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h3">30</p>
                            <p class="text-muted">Tổng số lần đo</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel mini-box">
                        <span class="box-icon bg-info">
                            <i class="fa fa-rocket"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h3">04/06</p>
                            <p class="text-muted">Ngày vượt nhiều nhất</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel mini-box">
                        <span class="box-icon bg-danger">
                            <i class="fa fa-user"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h3">5</p>
                            <p class="text-muted">Số lần vượt ngưỡng</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel mini-box">
                        <span class="box-icon bg-warning">
                            <i class="fa fa-rocket"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h3">Sáng</p>
                            <p class="text-muted">Buổi vượt nhiều nhất</p>
                        </div>
                    </div>
                </div>
<!--                <div class="col-sm-5">
                    <div class="panel mini-box">
                        <p class="size-h3 margin-b text-center">Recommend Advice</p>
                        <ul class="nav note-list">
                            <li>
                                <i class="fa fa-check-square-o"></i>
                                <strong>Rod</strong> uploaded 6 files. 
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                <br>
                                <small class="text-muted">about 4 hours ago</small>
                            </li>
                            <li>
                                <i class="fa fa-check-square-o"></i>
                                <strong>Rod</strong> uploaded 6 files. 
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                <br>
                                <small class="text-muted">about 4 hours ago</small>
                            </li>
                            <li>
                                <i class="fa fa-check-square-o"></i>
                                <strong>Rod</strong> uploaded 6 files. 
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                                <br>
                                <small class="text-muted">about 4 hours ago</small>
                            </li>
                        </ul>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>

<?/*<div class="col-sm-5">
                            <img src="/img/sample/donutchart.jpg" width="360px">
                            <h4>Record List</h4>
                            <div class="record-list perfectScrollbar margin-t">
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Sat, 25-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
<!--                                        <div class="round-icon bg-info">
                                            <span class="point1">201</span>
                                            <span class="point2">111</span>
                                            <span class="point3">75</span>
                                        </div>-->

<!--                                        <span class="round-icon bg-primary point1">201</span>
                                        <span class="round-icon bg-primary point2">111</span>
                                        <span class="round-icon bg-primary point3">76</span>-->
                                    </div>
                                    <div class="feed-subject">
                                        <!--<p>-->
<!--                                            <span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span>-->
                                            <!--<div class="btn-group">-->
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <!--</div>-->
                                            <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Morning</span>-->
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                        <!--</p>-->
                                    </div>
<!--                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>-->
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Afternoon</span>-->
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href=""><span class="round-icon bg-success"><i class="fa fa-smile-o"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">150</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">90</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">70</a>
                                        <!--<span class="badge badge-period"><i class="fa fa-clock-o fa-lg"></i> Evening</span>-->
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 0</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 25-05-2014 19:00"><i class="fa fa-clock-o"></i> 19:00</span>
                                    </div>
                                </div>
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Fri, 24-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 24-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 24-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item feed-date">
                                    <div class="feed-icon">
                                        <span class="round-icon"><i class="fa fa-sun-o"></i></span>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-primary">Thu, 23-05-2014</a>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension Emergency"><span class="round-icon bg-danger"><i class="fa fa-warning"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                            <a href="" class="btn btn-default" rel="tooltip" title="SYS">201</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="DIA">111</a>
                                            <a href="" class="btn btn-default" rel="tooltip" title="PULSE">80</a>
                                            <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 10</span></a>
                                            <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 07:00"><i class="fa fa-clock-o"></i> 07:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="" rel="tootip" title="Hypertension"><span class="round-icon bg-warning"><i class="fa fa-exclamation"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">170</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">100</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">75</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 5</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 13:00"><i class="fa fa-clock-o"></i> 13:00</span>
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href=""><span class="round-icon bg-success"><i class="fa fa-smile-o"></i></span></a>
                                    </div>
                                    <div class="feed-subject">
                                        <a href="" class="btn btn-default" rel="tooltip" title="SYS">150</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="DIA">90</a>
                                        <a href="" class="btn btn-default" rel="tooltip" title="PULSE">70</a>
                                        <a href="" rel="tooltip" title="Record Note"><span class="badge badge-primary"><i class="fa fa-comments fa-lg"></i> 0</span></a>
                                        <span class="pull-right text-muted" rel="tooltip" title="Sat, 23-05-2014 19:00"><i class="fa fa-clock-o"></i> 19:00</span>
                                    </div>
                                </div>
                            </div>
                        </div>*/?>
                        <?/*<div class="col-sm-7">
<!--                                    <img src="/img/sample/linechart3.jpg" >-->
                            <textarea rows="3" class="form-control margin-b" placeholder="Write message..."></textarea>
                            <div class="record-note perfectScrollbar">
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <!--<i class="fa fa-lightbulb-o"></i>-->
                                        <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Dr Trần Công Minh</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Nguyễn Tuấn Huy</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <!--<i class="fa fa-lightbulb-o"></i>-->
                                        <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Dr Trần Công Minh</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Nguyễn Tuấn Huy</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                            </div>
                        </div>*/?>

<?/*<script>
    $(function ($) { 
//        $("[data-toggle='tooltip']").tooltip(); 
//        $("[data-toggle='popover']").popover(); 
        $(".donutcharttt").popover({
            trigger: 'hover',
            placement: 'top',
            html: true,
            //content: '<?#php echo $this->renderPartial('//patient/_donutchart_tooltip', array(), true) ?>'
//            content: '<div class="flotTip"><h3>40%</h3><h5>Hypertension</h5></div>',
        }); 
        
        $(".linecharttt").popover({
            trigger: 'hover',
            placement: 'top',
            html: true,
//            content: '<div class="flotTip"><h3>180 | 135 | 75</h3><h5><i class="fa fa-clock-o"></i> 15:00 26-06-2014</h5></div>'
        }); 
    });
</script>*/?>

<!--            <div class="col-sm-5 record-note perfectScrollbar">
                <textarea rows="3" class="form-control margin-b" placeholder="Write message..."></textarea>
                <div class="callout callout-info">
                    <h4>Dr Trần Công Minh</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, alias, in accusantium totam adipisci vel et suscipit quidem libero pariatur minus ratione quo doloremque error at nemo incidunt dicta quia?</p>
                </div>
                <div class="callout callout-primary">
                    <h4>Nguyễn Tuấn Huy</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, alias, in accusantium totam adipisci vel et suscipit quidem libero pariatur minus ratione quo doloremque error at nemo incidunt dicta quia?</p>
                </div>
                <div class="callout callout-info">
                    <h4>Dr Trần Công Minh</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, alias, in accusantium totam adipisci vel et suscipit quidem libero pariatur minus ratione quo doloremque error at nemo incidunt dicta quia?</p>
                </div>
                <div class="callout callout-primary">
                    <h4>Nguyễn Tuấn Huy</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, alias, in accusantium totam adipisci vel et suscipit quidem libero pariatur minus ratione quo doloremque error at nemo incidunt dicta quia?</p>
                </div>
            </div>-->