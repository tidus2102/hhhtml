<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/patient.jpg"></a>
            <a href=""><h3>Nguyễn Tuấn Huy</h3></a>
            <ul class="nav nav-info">
                <li>Male, 60</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 infor">
            <div class="infoc">
                <a href=""><h3>Dr Trần Công Minh</h3></a>
                <ul class="nav nav-info">
                    <li>Male, 50</li>
                    <li>Hồ Chí Minh</li>
                </ul>
            </div>
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
        </div>
    </div>
</div>
<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li><a href="/patient/">Health</a></li>
            <li class="active"><a href="/patient/doctor">Doctor & Guardian</a></li>
            <!--<li><a href="/patient/guardian">Guardian</a></li>-->
            <!--<li><a href="/patient/community">Community</a></li>-->
<!--            
            <li class="active"><a href="/patient/health"><i class="fa fa-heart fa-lg"></i> Health</a></li>
            <li><a href="/patient/discussion"><i class="fa fa-comments fa-lg"></i> Discussion</a></li>
            <li><a href="/patient/doctor"><i class="fa fa-user-md fa-lg"></i> Doctor</a></li>
            <li><a href="/patient/guardian"><i class="fa fa-user fa-lg"></i> Guardian</a></li>
            <li><a href="/patient/community"><i class="fa fa-group fa-lg"></i> Community</a></li>-->
        </ul>
    </div>
</div>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="box-icon">
                <i class="fa fa-user-md fa-lg"></i>
            </span>
            Doctor & Guardian
        </div>
        <div class="panel-body">
            <div class="col-sm-5">
                <div class="panel panel-default panel-profile">
                    <div class="panel-heading text-center">
                        <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
                        <a href=""><span class="size-h3">Dr Trần Công Minh</span></a>
                        <!--<p>...</p>-->                    
                    </div>
                    <ul class="text-center">
                        <li>
                            <p class="size-h4">Heart</p>
                            <p class="text-muted">Specialties</p>
                        </li>
                        <li>
                            <p class="size-h4">20</p>
                            <p class="text-muted">Experience Years</p>
                        </li>
                        <li>
                            <p class="size-h4">115 Hopital</p>
                            <p class="text-muted">Work Office</p>
                        </li>
                    </ul>
                </div>
                
            </div>
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                             <i class="fa fa-group fa-lg"></i>
                        </span>
                        Guardians
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover user-list">
                                <thead>
                                    <tr>
                                        <th class="sorting">Guardian</th>
                                        <th>Relationship</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                            <a href="">Nguyễn Tuấn Minh</a>
                                        </td>
                                        <td>Son</td>
                                        <td><span class="label label-info">CONFIRM</span></td>
                                        <td><a href="">Remove</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user3.jpg"></a>
                                            <a href="">Nguyễn Minh Duy</a>
                                        </td>
                                        <td>Son</td>
                                        <td><span class="label label-info">CONFIRM</span></td>
                                        <td><a href="">Remove</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Guardian</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                             <i class="fa fa-user-md fa-lg"></i>
                        </span>
                        Doctor List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover user-list">
                                <thead>
                                    <tr>
                                        <th class="sorting">Doctor</th>
                                        <!--<th>Added Date</th>-->
                                        <!--<th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="img-thumbnail" src="/img/sample/doctor2.jpg"></a>
                                            <a href="">Dr Phan Mạnh Hùng</a>
                                        </td>
                                        <!--<td></td>-->
                                        <!--<td><span class="label label-info">CONFIRM</span></td>-->
                                        <td><a href="">Add as guardian</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="img-thumbnail" src="/img/sample/doctor3.jpg"></a>
                                            <a href="">Dr Lê Bích Trâm</a>
                                        </td>
                                        <!--<td></td>-->
                                        <!--<td><span class="label label-default">PENDING</span></td>-->
                                        <td><a href="">Add as guardian</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
        <!--                <div class="text-center">
                            <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Doctor</a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?/*<div class="panel mini-box">
                    <p class="size-h2 margin-b text-center">Doctor's Note</p>
                    <form role="form" class="form-horizontal margin-b">
                        <textarea rows="3" class="form-control" placeholder="Add note"></textarea>
                    </form>
                    <ul class="nav note-list">
                        <li>
                            <i class="fa fa-check-square-o"></i>
        <!--                    <strong>Rod</strong> uploaded 6 files. -->
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                            <br>
                            <small class="text-muted">
                                about 4 hours ago
                                <a href="" class="pull-right">Delete</a>
                            </small>
                        </li>
                        <li>
                            <i class="fa fa-check-square-o"></i>
        <!--                    <strong>Rod</strong> uploaded 6 files. -->
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                            <br>
                            <small class="text-muted">
                                about 4 hours ago
                                <a href="" class="pull-right">Delete</a>
                            </small>
                        </li>
                        <li>
                            <i class="fa fa-check-square-o"></i>
        <!--                    <strong>Rod</strong> uploaded 6 files. -->
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                            <br>
                            <small class="text-muted">
                                about 4 hours ago
                                <a href="" class="pull-right">Delete</a>
                            </small>
                        </li>
                    </ul>
                </div>*/?>

<?/*<div class="col-sm-7">
                <div class="action-box">
                    <textarea placeholder="Create your topic..." rows="3" class="form-control"></textarea>
                    <div class="action-list">
                        <div class="action-item">
                            <a title="" class="fa fa-picture-o" rel="tooltip" data-original-title="Post an Image"><i></i></a>
                            <a title="" class="fa fa-video-camera" rel="tooltip" data-original-title="Upload a Video"><i></i></a>
        <!--                    <a title="" class="fa fa-lightbulb-o" rel="tooltip" data-original-title="Post an Idea"><i></i></a>
                            <a title="" class="fa fa-question-circle" rel="tooltip" data-original-title="Ask a Question"><i></i></a>-->
                        </div>	
                        <div class="pull-right">
                            <a class="btn btn-primary btn-sm">Post</a>
                        </div>
                    </div>
                </div>
                <div class="margin-b">
                    <div class="feed-item">
                        <div class="feed-icon">
                            <!--<i class="fa fa-lightbulb-o"></i>-->
                            <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                        </div>
                        <div class="feed-subject">
                            <p>
                                <a href="#">Dr Trần Công Minh</a>
                                <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                            </p>
                        </div>
                        <div class="feed-content">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                        </div>
                        <div class="feed-actions">
            <!--                <a class="pull-left" rel="tooltip"><i class="fa fa-thumbs-up"></i> 123</a> 
                            <a class="pull-left" rel="tooltip"><i class="fa fa-comment-o"></i> 29</a>-->
                            <a class="pull-right" href="#">Reply</a>
                        </div>
                        <div class="feed-item">
                            <div class="feed-icon">
                                <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                            </div>
                            <div class="feed-subject">
                                <p>
                                    <a href="#">Nguyễn Tuấn Huy</a>
                                    <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                </p>
                            </div>
                            <div class="feed-content">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                            </div>
                        </div>
                    </div>
                    <div class="feed-item">
                        <div class="feed-icon">
                            <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                        </div>
                        <div class="feed-subject">
                            <p>
                                <a href="#">Nguyễn Tuấn Huy</a>
                                <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                            </p>
                        </div>
                        <div class="feed-content">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                        </div>
                        <div class="feed-actions">
            <!--                <a class="pull-left" rel="tooltip"><i class="fa fa-thumbs-up"></i> 123</a> 
                            <a class="pull-left" rel="tooltip"><i class="fa fa-comment-o"></i> 29</a>-->
                            <a class="pull-right" href="#">Reply</a>
                        </div>
                    </div>
                </div>
            </div>*/?>