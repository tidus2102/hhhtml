<div class="container margin-t">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Notifications
            </div>
            <div class="panel-body">
                <div class="feed-item">
                    <div class="feed-icon">
                        <!--<i class="fa fa-lightbulb-o"></i>-->
                        <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                    </div>
                    <div class="feed-subject">
                        <p>
                            <a href="">Dr Trần Công Minh</a> add <a href="">note</a> on your record
                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 20 minutes ago</span>
                        </p>
                    </div>
        <!--            <div class="feed-content">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                    </div>-->
                </div>
                <div class="feed-item">
                    <div class="feed-icon">
                        <!--<i class="fa fa-lightbulb-o"></i>-->
                        <a href="#"><img src="/img/sample/son.jpg" class="img-thumbnail"></a>
                    </div>
                    <div class="feed-subject">
                        <p>
                            <a href="">Nguyễn Tuấn Minh</a> become your guardian
                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 25 minutes ago</span>
                        </p>
                    </div>
        <!--            <div class="feed-content">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>