<div class="container">
    <h1 class="text-center">Account</h1>
    <div class="margin-t">
        <ul class="nav nav-pills " data-tabs="tabs">
            <li class="active"><a data-toggle="tab" href="#s1"><i class="fa fa-navicon"></i> General</a></li>
            <li><a data-toggle="tab" href="#s2"><i class="fa fa-header"></i> Device</a></li>
            <li><a data-toggle="tab" href="#s3"><i class="fa fa-cog"></i> Setting</a></li>
        </ul>
    </div>
    <div class="tab-content ">
        <div id="s1" class="tab-pane active">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="box-icon">
                        <i class="fa fa-user fa-lg"></i>
                    </span>
                    Information
                </div>  
                <div class="panel-body text-center">
                    <div class="col-sm-5">
                        <img src="/img/sample/patient.jpg" class="img-thumbnail img-circle avatar-large">
                        <div class="clearfix"></div>
                        <a class="btn btn-primary margin-t" href="">Change Avatar</a>
                    </div>
                    <div class="col-sm-7">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Birthday</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9 radio-inline">
                                    <label class="ui-radio"><input type="radio" name="gender" value="" checked="true"><span>Male</span></label>
                                    <label class="ui-radio"><input type="radio" name="gender" value=""><span>Female</span></label>
                                    <label class="ui-radio"><input type="radio" name="gender" value=""><span>Unknown</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Identity Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <div class="col-sm-3">
                                    <input type="submit" class="btn btn-primary" value="Update Info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
        <div id="s2" class="tab-pane">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="box-icon">
                        <i class="fa fa-mobile fa-lg"></i>
                    </span>
                    Blood Pressure Monitor - BP0001
                </div>  
                <div class="panel-body">
<!--                    <div id="accordion1" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#patient1" data-parent="#accordion1" data-toggle="collapse" class="collapsed">
                                        <img class="img-thumbnail" src="/img/sample/bpdevice.jpg">
                                        BP0001
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="patient1">
                                <div class="panel-body">-->
                                    <div class="col-sm-4">
                                        <div class="text-center">
                                            <img class="img-thumbnail avatar-large" src="/img/sample/bpdevice.jpg">
                                        </div>
                                        <ul class="nav nav-list list-info margin-t">
                                            <li class="ng-binding">
                                                <span class="icon glyphicon glyphicon-user"></span>
                                                <label>Name</label>
                                                Blood Presure Monitor
                                            </li>
                                            <li>
                                                <span class="icon glyphicon glyphicon-envelope"></span>
                                                <label>Serial</label>
                                                BP001
                                            </li>
                                            <li>
                                                <span class="icon glyphicon glyphicon-home"></span>
                                                <label>Buy Date</label>
                                                13-06-2014
                                            </li>
                                            <li>
                                                <span class="icon glyphicon glyphicon-earphone"></span>
                                                <label>Warranty</label>
                                                5 years
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead>
                                                    <tr>
                                                        <th class="sorting">User</th>
                                                        <th>Account Code</th>
                                                        <th>Added Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
                                                        <td class="text-left">
                                                            <a href=""><img class="img-thumbnail" src="/img/sample/son.jpg"></a>
                                                            <a href="">Nguyễn Tuấn Minh</a>
                                                        </td>
                                                        <td><b>0002</b></td>
                                                        <td>13-06-2014</td>
                                                        <td><span class="label label-info">ACTIVE</span></td>
                                                        <td><a href="">Remove</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add User</a>
                                        </div>
                                    </div>
<!--                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
        <div id="s3" class="tab-pane">
<!--            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="box-icon">
                        <i class="fa fa-cogs fa-lg"></i>
                    </span>
                    Setting
                </div>  
                <div class="panel-body">-->
                    <div class="col-sm-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="box-icon">
                                    <i class="fa fa-lock fa-lg"></i>
                                </span>
                                Password
                            </div>
                            <div class="panel-body">
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Current</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">New</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Confirm</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group pull-right">
                                        <div class="col-sm-3">
                                            <input type="submit" class="btn btn-primary" value="Change Password">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="box-icon">
                                    <i class="fa fa-lock fa-lg"></i>
                                </span>
                                Setting
                            </div>
                            <div class="panel-body">
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label switch-label">Receive notifications via email</label>
                                        <div class="col-sm-7 radio-inline">
                                            <!--<label class="ui-checkbox"><input type="checkbox" name="" value="" checked="true"><span>Receive notifications via Email</span></label>-->
                                            <label class="switch"><input type="checkbox" unchecked="true"><i></i></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label switch-label">Receive notifications via SMS</label>
                                        <div class="col-sm-7 radio-inline">
                                            <label class="switch"><input type="checkbox" checked="true"><i></i></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label switch-label">Receive news via email</label>
                                        <div class="col-sm-7 radio-inline">
                                            <label class="switch"><input type="checkbox" checked="true"><i></i></label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<!--                </div>
            </div>-->
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add User</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" placeholder="Type name...">
                        </div>
                    </div>
                </form>
                <table class="table table-hover user-list">
                    <tbody>
						<tr>
							<td>
								<a href=""><img class="img-thumbnail" src="/img/sample/user1.jpg"></a>
								<a href="">Trần Thanh Thúy</a>
<!--                                    <ul class="nav nav-list">
									<li>Specialties: Heart</li>
									<li>Experience: 15</li>
								</ul>-->
							</td>          
							<td class="text-center">
								<a href="" class="btn btn-primary">Add</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href=""><img class="img-thumbnail" src="/img/sample/user2.jpg"></a>
								<a href="">Lê Huy</a>
<!--                                    <ul class="nav nav-list">
									<li>Specialties: Heart</li>
									<li>Experience: 15</li>
								</ul>-->
							</td>          
							<td class="text-center">
								<a href="" class="btn btn-primary">Add</a>
							</td>
						</tr>
					</tbody>
                </table>
            </div>
        </div>
    </div>
</div>