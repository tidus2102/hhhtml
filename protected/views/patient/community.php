<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/patient.jpg"></a>
            <a href=""><h3>Nguyễn Tuấn Huy</h3></a>
            <ul class="nav nav-info">
                <li>Male, 60</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 infor">
            <div class="infoc">
                <a href=""><h3>Dr Trần Công Minh</h3></a>
                <ul class="nav nav-info">
                    <li>Male, 50</li>
                    <li>Hồ Chí Minh</li>
                </ul>
            </div>
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
        </div>
    </div>
</div>
<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li><a href="/patient/health">Health</a></li>
            <li><a href="/patient/doctor">Doctor</a></li>
            <li><a href="/patient/guardian">Guardian</a></li>
            <li class="active"><a href="/patient/community">Community</a></li>
<!--            
            <li class="active"><a href="/patient/health"><i class="fa fa-heart fa-lg"></i> Health</a></li>
            <li><a href="/patient/discussion"><i class="fa fa-comments fa-lg"></i> Discussion</a></li>
            <li><a href="/patient/doctor"><i class="fa fa-user-md fa-lg"></i> Doctor</a></li>
            <li><a href="/patient/guardian"><i class="fa fa-user fa-lg"></i> Guardian</a></li>
            <li><a href="/patient/community"><i class="fa fa-group fa-lg"></i> Community</a></li>-->
        </ul>
    </div>
</div>
<div class="container">
    <div class="col-sm-5">
    </div>
    <div class="col-sm-7">
    </div>
</div>