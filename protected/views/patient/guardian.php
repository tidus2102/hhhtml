<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/patient.jpg"></a>
            <a href=""><h3>Nguyễn Tuấn Huy</h3></a>
            <ul class="nav nav-info">
                <li>Male, 60</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 infor">
            <div class="infoc">
                <a href=""><h3>Dr Trần Công Minh</h3></a>
                <ul class="nav nav-info">
                    <li>Male, 50</li>
                    <li>Hồ Chí Minh</li>
                </ul>
            </div>
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
        </div>
    </div>
</div>
<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li><a href="/patient/health">Health</a></li>
            <li><a href="/patient/doctor">Doctor</a></li>
            <li class="active"><a href="/patient/guardian">Guardian</a></li>
            <li><a href="/patient/community">Community</a></li>
<!--            
            <li class="active"><a href="/patient/health"><i class="fa fa-heart fa-lg"></i> Health</a></li>
            <li><a href="/patient/discussion"><i class="fa fa-comments fa-lg"></i> Discussion</a></li>
            <li><a href="/patient/doctor"><i class="fa fa-user-md fa-lg"></i> Doctor</a></li>
            <li><a href="/patient/guardian"><i class="fa fa-user fa-lg"></i> Guardian</a></li>
            <li><a href="/patient/community"><i class="fa fa-group fa-lg"></i> Community</a></li>-->
        </ul>
    </div>
</div>
<div class="container">
    <?/*<div class="col-sm-5">
        <div class="">
            <div class="panel panel-profile">
                <div class="panel-heading text-center">
                    <a href=""><img class="img-circle img-thumbnail avatar" src="/img/sample/doctor.jpg"></a>
                    <a href=""><span class="size-h3">Dr Trần Công Minh</span></a>
                    <!--<p>...</p>-->                    
                </div>
                <ul class="text-center">
                    <li>
                        <p class="size-h4">Heart</p>
                        <p class="text-muted">Specialties</p>
                    </li>
                    <li>
                        <p class="size-h4">20</p>
                        <p class="text-muted">Experience Years</p>
                    </li>
                    <li>
                        <p class="size-h4">115 Hopital</p>
                        <p class="text-muted">Work Office</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>*/?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="box-icon">
                 <i class="fa fa-group fa-lg"></i>
            </span>
            Guardian List
        </div>
        <div class="panel-body">
            <div class="col-sm-7">
                <div class="table-responsive">
                    <table class="table table-hover user-list">
                        <thead>
                            <tr>
                                <th class="sorting">Guardian</th>
                                <th>Relationship</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                    <a href="">Nguyễn Tuấn Minh</a>
                                </td>
                                <td>Son</td>
                                <td><span class="label label-info">CONFIRM</span></td>
                                <td><a href="">Remove</a></td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user3.jpg"></a>
                                    <a href="">Nguyễn Minh Duy</a>
                                </td>
                                <td>Son</td>
                                <td><span class="label label-info">CONFIRM</span></td>
                                <td><a href="">Remove</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Guardian</a>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                             <i class="fa fa-user-md fa-lg"></i>
                        </span>
                        Doctor List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover user-list">
                                <thead>
                                    <tr>
                                        <th class="sorting">Doctor</th>
                                        <!--<th>Added Date</th>-->
                                        <!--<th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="img-thumbnail" src="/img/sample/doctor2.jpg"></a>
                                            <a href="">Dr Phan Mạnh Hùng</a>
                                        </td>
                                        <!--<td></td>-->
                                        <!--<td><span class="label label-info">CONFIRM</span></td>-->
                                        <td><a href="">Add as guardian</a></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <a href=""><img class="img-thumbnail" src="/img/sample/doctor3.jpg"></a>
                                            <a href="">Dr Lê Bích Trâm</a>
                                        </td>
                                        <!--<td></td>-->
                                        <!--<td><span class="label label-default">PENDING</span></td>-->
                                        <td><a href="">Add as guardian</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
        <!--                <div class="text-center">
                            <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Doctor</a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?/*
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Doctor</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="Type doctor name...">
                        </div>
                        <div class="checkbox col-sm-3">
                            <label class="ui-checkbox"><input type="checkbox" value="option1" name="checkbox1"><span>Main Doctor</span></label>
                        </div>
<!--                        <div class="radio col-sm-6">
                            <label class="ui-radio"><input type="radio" value="option1" name="radio1"><span>Option 1</span></label>
                            <label class="ui-radio"><input type="radio" value="option2" name="radio1"><span>Option 2</span></label>
                        </div>-->
                    </div>
                </form>
                <!--<div class="search-result">-->
                    <table class="table table-hover user-list">
                        <tbody>
                            <tr>
                                <td>
                                    <a href=""><img src="/img/sample/doctor4.jpg" class="img-thumbnail"></a>
                                    <a href="">Dr Nguyễn Thế Vinh</a>
<!--                                    <ul class="nav nav-list">
                                        <li>Specialties: Heart</li>
                                        <li>Experience: 15</li>
                                    </ul>-->
                                </td>          
                                <td class="text-center">
                                    <a class="btn btn-primary" href="">Add</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href=""><img src="/img/sample/doctor5.jpg" class="img-thumbnail"></a>
                                    <a href="">Dr Phan Văn Hải</a>
<!--                                    <ul class="nav nav-list">
                                        <li>Specialties: Heart</li>
                                        <li>Experience: 15</li>
                                    </ul>-->
                                </td>          
                                <td class="text-center">
                                    <a class="btn btn-primary" href="">Add</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <!--</div>-->
            </div>
<!--            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>
*/?>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Guardian</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Type name...">
                        </div>
                        <!--<label class="col-sm-2 control-label">Relationship</label>-->
                        <div class="col-sm-4">
                            <select class=" form-control">
                                <option>Relationship</option>
                                <option>Father</option>
                                <option>Mother</option>
                                <option>Uncle</option>
                                <option>Aunt</option>
                                <option>Husband</option>
                                <option>Wife</option>
                                <option>Son</option>
                                <option>Daughter</option>
                                <option>Friend</option>
                                <option>Other</option>
                            </select>
                        </div>
<!--                        <div class="radio col-sm-6">
                            <label class="ui-radio"><input type="radio" value="option1" name="radio1"><span>Option 1</span></label>
                            <label class="ui-radio"><input type="radio" value="option2" name="radio1"><span>Option 2</span></label>
                        </div>-->
                    </div>
                </form>
                <!--<div class="search-result">-->
                    <table class="table table-hover user-list">
                        <tbody>
                            <tr>
                                <td>
                                    <a href=""><img src="/img/sample/user1.jpg" class="img-thumbnail"></a>
                                    <a href="">Trần Thanh Thúy</a>
<!--                                    <ul class="nav nav-list">
                                        <li>Specialties: Heart</li>
                                        <li>Experience: 15</li>
                                    </ul>-->
                                </td>          
                                <td class="text-center">
                                    <a class="btn btn-primary" href="">Add</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href=""><img src="/img/sample/user2.jpg" class="img-thumbnail"></a>
                                    <a href="">Lê Huy</a>
<!--                                    <ul class="nav nav-list">
                                        <li>Specialties: Heart</li>
                                        <li>Experience: 15</li>
                                    </ul>-->
                                </td>          
                                <td class="text-center">
                                    <a class="btn btn-primary" href="">Add</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href=""><img src="/img/sample/user3.jpg" class="img-thumbnail"></a>
                                    <a href="">Nguyễn Minh Duy</a>
<!--                                    <ul class="nav nav-list">
                                        <li>Specialties: Heart</li>
                                        <li>Experience: 15</li>
                                    </ul>-->
                                </td>          
                                <td class="text-center">
                                    <a class="btn btn-primary" href="">Add</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <!--</div>-->
            </div>
<!--            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>