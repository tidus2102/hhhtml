<div class="intro-bg">
    <div class="intro-block">
        <div class="container">
            <h4 class="text-center">Wecome to HelloHealth</h4>
            <div class="col-sm-offset-4 col-sm-4 col-xs-offset-3 col-sm-6">
                <div class="panel panel-default">
<!--                    <div class="panel-heading text-center">
                        <span class="box-icon">
                            <i class="fa fa-h-square fa-lg"></i>
                        </span>
                        
                    </div>-->
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" id="form-login">
<!--                            <div class="form-group">
                                <div class="input-group col-sm-12">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" placeholder="Email or Phone" >
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Email or Phone" class="form-control left-icon">
                                    <i class="fa fa-envelope left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control left-icon" placeholder="Password" >
                                    <i class="fa fa-lock left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
<!--                                <div class="col-sm-12">
                                    <button class="btn btn-primary btn-block" type="submit">SIGN IN</button>
                                </div>-->
                                <div class="text-center">
                                    <a class="btn btn-primary" href="/user/index">Normal</a>
                                    <a class="btn btn-primary" href="/patient/index">Patient</a>
                                    <a class="btn btn-primary" href="/doctor/index">Doctor</a>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <a href="/forgotpassword" class="text-muted">Forgot Password</a>
                            </div>
<!--                            <div class="form-group text-center">
                                Not a member? <a href="/signup" class="">Sign up now</a>
                            </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<script>
    $(document).on('click', '#show-signup', function(event){
        event.preventDefault();
        $('#form-login').addClass('hide');
        $('#register-form').removeClass('hide');
    });
    
    $(document).on('click', '#show-login', function(event){
        event.preventDefault();
        $('#form-login').removeClass('hide');
        $('#register-form').addClass('hide');
    });
</script>-->