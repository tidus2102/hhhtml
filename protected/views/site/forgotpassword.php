<div class="container margin-t">
    <div class="col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading text-center">
<!--                <span class="box-icon">
                    <i class="fa fa-recycle fa-lg"></i>
                </span>-->
                <span class="size-h4">HelloHealth</span>
            </div>
            <div class="panel-body">
                <div class="text-center">
                    <h4>Forgot Password?</h4>
                    <!--Don't worry, we'll send you an email to reset your password.-->
                    Don't worry, we'll send your new password to your email.
                </div>
                <form role="form" class="margin-t">
                    <div class="form-group">
                        <!--<label class="control-label">Email</label>-->
                        <div class="">
                            <input type="text" class="form-control" placeholder="Your Email">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="text-right">
                            <input type="submit" class="btn btn-primary" value="Reset Password">
                        </div>
                    </div>
                </form>
                <div class="alert alert-success margin-t">
                    Your new password has been sent to your email! You can <a href="/" class="">log in</a> now!
                </div>
            </div>
        </div>
    </div>
</div>