﻿<div class="container">
    <div class="col-sm-12 text-center">
        <h3>Huyết áp - Tim mạch</h3>
    </div>
    <div class="col-sm-8 margin-t article-list">
        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                <a href="/articleDetails">Huyết áp là gì?</a>
            </div>
            <div class="panel-body">
                <img src="/img/article/1do-huyet-ap-190x130.jpg" class="img-thumbnail">
                Có 2 vấn đề sức khỏe đang gây nhức nhối với cộng đồng đó là huyết áp cao và huyết áp thấp. Người ta nhắc nhiều đến các chỉ số “huyết áp” kèm theo thông điệp theo dõi chỉ số huyết áp như theo dõi chính chỉ số sức khỏe của bạn và gia đình. Vậy huyết áp là gì? <a href="/articleDetails">Doc them</a>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                <a href="/articleDetails">Nhận biết sớm các triệu chứng huyết áp cao</a>
            </div>
            <div class="panel-body">
                <img src="/img/article/2816487-190x130.jpeg" class="img-thumbnail">
                Huyết áp cao vô cùng nguy hiểm bởi nó có thể gây ra những ảnh hưởng nghiêm trọng tới sức khỏe và chất lượng cuộc sống. Vì vậy, người bệnh cũng như người thân cần phải trang bị cho mình những hiểu biết nhất định để nhận biết đúng các triệu chứng huyết áp cao, phát hiện và xử trí kịp thời tránh những hậu quả đáng tiếc xảy ra <a href="/articleDetails">Doc them</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4 margin-t">
        <div class="alert alert-success text-center margin-b">
            <span class="size-h4">
                The first 100 pre-orders get <b>FREE</b> Tele-Blood Pressure Monitor!
                <br>
                Only <b>20</b> remains! <br>
                <a href="/#pricing">PRE-ORDER now!</a>
            </span>
        </div>
<!--        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                Khuyen mai
            </div>
            <div class="panel-body">
        
            </div>
        </div>-->
        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                Cam nang suc khoe
            </div>
            <div class="panel-body">
                <ul class="nav nav-list">
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>