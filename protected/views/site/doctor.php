<div class="container">
    <h1 class="text-center">Doctor List</h1>
    <div class="col-sm-3 ">
<!--        <div class="alert alert-success text-center margin-b">
            <span class="size-h4">
                The first 100 pre-orders get <b>FREE</b> Tele-Blood Pressure Monitor!
                <br>
                Only <b>20</b> remains! <br>
                <a href="/#pricing">PRE-ORDER now!</a>
            </span>
        </div>-->
        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                Chuyên khoa
            </div>
            <div class="panel-body">
                <ul class="nav nav-list">
                    <li><a>Tim mạch</a></li>
                    <li><a>Tổng quát</a></li>
                    <li><a>Xét nghiệm</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-9 doctor-list">
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctorg.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">TRẦN THỊ NHƯ HOA</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Tim mạch</td>
                            <td><b>Vị trí hiện tại</b>: Trưởng khoa Cấp cứu - Tim mạch</td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 20 năm</td>
                            <td><b>Nơi làm việc</b>: Phòng khám Quốc tế Victoria Healthcare</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">TRẦN THANH SƠN</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Tim mạch</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ chuyên khoa tim</td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 14 năm</td>
                            <td><b>Nơi làm việc</b>: Phòng khám Quốc tế Victoria Healthcare</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">NGUYỄN CẢNH NAM</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Nội thần kinh</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ khoa nội thần kinh</td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 11 năm</td>
                            <td><b>Nơi làm việc</b>: Phòng khám Quốc tế Victoria Healthcare</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">PHAN HỮU TÚ</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Nội tổng quát</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ chuyên khoa Nội tiết</td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 16 năm</td>
                            <td><b>Nơi làm việc</b>: Phòng khám Quốc tế Victoria Healthcare</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctorg.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">TRẦN PHƯƠNG THẢO</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Tim mạch</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ chuyên khoa tim </td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 11 năm</td>
                            <td><b>Nơi làm việc</b>: Phòng khám Quốc tế Victoria Healthcare</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">NGUYỄN ĐỨC CHỈNH</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Tim mạch</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ chuyên khoa tim </td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 11 năm</td>
                            <td><b>Nơi làm việc</b>: Đại học Đại học Y Dược TP. HCM </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default margin-b">
            <div class="panel-body">
                <img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle">
                <p><a><span class="size-h4">TRẦN LÊ VŨ</span></a></p>
                <table class="doctor-detail">
                    <tbody>
                        <tr>
                            <td><b>Chuyên khoa</b>: Tim mạch</td>
                            <td><b>Vị trí hiện tại</b>: Bác sĩ chuyên khoa tim </td>
                        </tr>
                        <tr>
                            <td><b>Kinh nghiệm</b>: 11 năm</td>
                            <td><b>Nơi làm việc</b>: Đại học Đại học Y Dược TP. HCM </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>