<div class="container">
    <h1 class="text-center">CHECK OUT</h1>
    <div class="margin-t">
        <div class="col-sm-4">
<!--            <h2>PLAN INFORMATION</h2>-->
            <div class="panel panel-default plan">
                <div class="panel-heading">
                    <span class="box-icon">
                        <i class="fa fa-tasks fa-lg"></i>
                    </span>
                    ORDER
                </div>  
                <div class="panel-body ">
                    <div class="text-center">
                        <h3>Standard</h3>
                        <span class="size-h1" id="total-price">$150</span> <span class="text-muted">/ year</span>
                    </div>
                    <ul class="nav nav-list list-info margin-t">
                        <li>
                            <span class="icon glyphicon glyphicon-phone"></span>
                            <label>Device</label>
                            Blood Pressure Monitor
                        </li>
                        <li>
                            <span class="icon glyphicon glyphicon-home"></span>
                            <label>Warranty</label>
                            3 years
                        </li>
                        <li>
                            <span class="icon glyphicon glyphicon-user"></span>
                            <label>Account</label>
                            1 account
                        </li>
                        
                    </ul>
                    <hr>
                    <form role="form" class="form-horizontal margin-t">
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Warranty Period</label>
                            <div class="col-sm-6">
                                <select class="form-control">
                                    <option>3 years</option>
                                    <option>5 years</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">Account Quantity</label>
                            <div class="col-sm-6">
                                <select class="form-control">
                                    <option>1 account</option>
                                    <option>2 accounts</option>
                                    <option>3 accounts</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-center">
                                <input type="submit" class="btn btn-info" value="Update Order" id="update-order">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div id="wizard">
                <h1 class="hide">ACCOUNT</h1>
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="box-icon">
                                <i class="fa fa-user fa-lg"></i>
                            </span>
                            ACCOUNT
                        </div>  
                        <div class="panel-body">
                            <div class="col-sm-offset-2 col-sm-8">
                                <h3 class="text-center">Sign In</h3>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Email</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Password</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 text-right">
                                            <input type="submit" value="Sign In" class="btn btn-primary ">
                                        </div>
                                    </div>
                                </form>
                                <h3 class="text-center">Create new account</h3>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Name</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Email</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Password</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 text-right">
                                            <input type="submit" class="btn btn-info" value="Sign Up">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <?/*<h1 class="hide">PAYMENT</h1>
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="box-icon">
                                <i class="fa fa-money fa-lg"></i>
                            </span>
                            PAYMENT
                        </div>  
                        <div class="panel-body">
                            <div class="col-sm-offset-1 col-sm-10">
                                <h3 class="text-center">Payment method</h3>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <!--<label class="col-sm-12 ">Choose payment method</label>-->
                                        <div class="col-sm-12">
                                            <label class="ui-radio"><input type="radio" name="payment" value="" ><span>Cash On Delivery</span></label>
                                            <label class="ui-radio"><input type="radio" name="payment" value=""><span>Internet Banking</span></label>
                                            <label class="ui-radio"><input type="radio" name="payment" value="" checked="true"><span>VISA, Master Card</span></label>
                                        </div>
                                    </div>
                                </form>
                                <h3 class="text-center">Payment information</h3>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Card Number</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Card Number</label>
                                        <div class="col-sm-8 radio-inline">
                                            <label class="ui-radio"><input type="radio" name="card" value=""><span>VISA</span></label>
                                            <label class="ui-radio"><input type="radio" name="card" value="" checked="true"><span>Master Card</span></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Expired Date</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>Month</option>
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                                <option>05</option>
                                                <option>06</option>
                                                <option>07</option>
                                                <option>08</option>
                                                <option>09</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>Year</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                                <option>2018</option>
                                                <option>2019</option>
                                                <option>2020</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Security Code</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Ward</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Province/City</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" value="" placeholder="">
                                        </div>
<!--                                        <div class="col-sm-12">
                                            <select class="form-control">
                                                <option>Choose Province/City</option>
                                                <option>Hồ Chí Minh</option>
                                                <option>Hà Nội</option>
                                                <option>Đà Nẵng</option>
                                                <option>Khánh Hòa</option>
                                                <option>Lâm Đồng</option>
                                                <option>Bình Dương</option>
                                            </select>
                                        </div>-->
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Country</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Choose Country</option>
                                                <option>Việt Nam</option>
                                                <option>Singapore</option>
                                                <option>Thailand</option>
                                                <option>Hong Kong</option>
                                                <option>Taiwan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 text-right">
                                            <input type="submit" class="btn btn-primary" value="Payment">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>*/?>
              
                <h1 class="hide">PAYMENT</h1>
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="box-icon">
                                <i class="fa fa-car fa-lg"></i>
                            </span>
                            PAYMENT
                        </div>  
                        <div class="panel-body">
                            <div class="col-sm-offset-2 col-sm-8">
                                <h3 class="text-center">Cash On Delivery</h3>
                                <h3 class="text-center">Delivery information</h3>
                                <form role="form" class="form-horizontal">
<!--                                    <div class="form-group">
                                        <div class="checkbox col-sm-12">
                                            <label class="ui-checkbox"><input type="checkbox" value="option1" name="checkbox1" unchecked=""><span>Use account info</span></label>
                                        </div>
                                    </div>-->
<!--                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" disabled="" value="Tạ Minh Tuấn">
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Name" value="Tạ Minh Tuấn">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Phone</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Phone" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Province/City</label>-->
                                        <div class="col-sm-12">
                                            <select class="form-control">
                                                <option>Choose Province/City</option>
                                                <option>Hồ Chí Minh</option>
                                                <option>Hà Nội</option>
                                                <option>Đà Nẵng</option>
                                                <option>Khánh Hòa</option>
                                                <option>Lâm Đồng</option>
                                                <option>Bình Dương</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Address</label>-->
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Address</label>-->
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="3" placeholder="Note"></textarea>
                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <div class="col-sm-12 text-right">
                                            <input type="submit" class="btn btn-primary" value="Continue">
                                        </div>
                                    </div>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <h1 class="hide">FINISH</h1>
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="box-icon">
                                <i class="fa fa-car fa-lg"></i>
                            </span>
                            FINISH
                        </div>  
                        <div class="panel-body">
                            <div class="col-sm-offset-2 col-sm-8">
                                <div class="alert alert-default text-center">
                                    <p class="size-h3">Order successful!</p>
                                    Thank you. We will contact you soon!
                                </div>
                                <h3 class="text-center">Account use device</h3>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="checkbox col-sm-12">
                                            <label class="ui-checkbox"><input type="checkbox" value="option1" name="checkbox1" checked=""><span>Tạ Minh Tuấn (I use device)</span></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox col-sm-12">
                                            <label class="ui-checkbox"><input type="checkbox" value="option2" name="checkbox2" checked=""><span>Nguyễn Minh A</span></label>
                                        </div>
                                    </div>
                                </form>
                                <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Name" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Address</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Email" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--<label class="col-sm-2 control-label">Phone</label>-->
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 text-right">
                                            <input type="submit" class="btn btn-primary" value="Add">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#wizard").steps({
//        onFinished: function (event, currentIndex) { 
//                window.location = "/finish";
//        }
    });
    
    
//    $(document).on('click', '#update-order', function(event){
//        event.preventDefault();
//        var w = $("#total-price").val();
//        var ml = w * 2.20 / 2 * 29.57;
//
//        var result = ml.toFixed(0);
//        $("#water").text(addCommas(result.toString()));
//
//        var bottle = (result / 590).toFixed(0);
//        $("#total-price").text(bottle.toString());
//
//        $("#result").removeClass("hide");
//    });
</script>