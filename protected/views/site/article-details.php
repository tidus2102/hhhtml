﻿<div class="container">
    <div class="col-sm-12 text-center">
        <h3>Blood Pressure</h3>
    </div>
    <div class="col-sm-8 margin-t article-list">
        <h4 class="red">Huyết áp - Tim mạch</h4>
        <div class="article-content">
            Có 2 vấn đề sức khỏe đang gây nhức nhối với cộng đồng đó là huyết áp cao và huyết áp thấp. Người ta nhắc nhiều đến các chỉ số “huyết áp” kèm theo thông điệp theo dõi chỉ số huyết áp như theo dõi chính chỉ số sức khỏe của bạn và gia đình. Vậy huyết áp là gì? Thế nào được coi là huyết áp cao và huyết áp thấp? Mời bạn đọc cùng tham khảo một số thông tin bổ ích về huyết áp qua bài viết dưới đây.<br>
            <br>
            <p class="text-center"><img src="/img/article/1HuyetAp.jpg"></p>
                Huyết áp là gì ?<br>
<br>
                Huyết áp là áp lực máu cần thiết tác động lên thành động mạch nhằm đưa máu đến nuôi dưỡng các mô trong cơ thể. Huyết áp được tạo ra do lực co bóp của tim và sức cản của động mạch.<br>
<br>
                Ở người bình thường, huyết áp ban ngày cao hơn ban đêm, huyết áp hạ xuống thấp nhất vào khoảng 1-3 giờ sáng khi ngủ say và huyết áp cao nhất từ 8 – 10 giờ sáng. Khi vận động, gắng sức thể lực, căng thẳng thần kinh hoặc khi xúc động mạnh đều có thể làm huyết áp tăng lên. Và ngược lại, khi cơ thể được nghỉ ngơi, thư giãn, huyết áp có thể hạ xuống.<br>
<br>
                Khi bị lạnh gây co mạch, hoặc dùng một số thuốc co mạch hoặc thuốc co bóp cơ tim, ăn mặn có thể làm huyết áp tăng lên. Ở môi trường nóng, ra nhiều mồ hôi, bị tiêu chảy … hoặc dùng thuốc giãn mạch có thể gây hạ huyết áp.<br>
<br>
                Huyết áp được thể hiện bằng 2 chỉ số:<br>
<br>
                Huyết áp tối đa (còn gọi là huyết áp tâm thu hoặc ngắn gọn là số trên), bình thường từ 90 đến 139 mm Hg (đọc là milimét thuỷ ngân).<br>
                Huyết áp tối thiểu (còn gọi là huyết áp tâm trương hoặc ngắn gọn là số dưới), bình thường từ 60 đến 89 mm Hg.<br>
                Mỗi người phải luôn biết và nhớ hai chỉ số huyết áp của mình.<br>
<br>
                Thế nào là huyết áp cao và huyết áp thấp?<br>
<br>
                Trên thực tế, cả 2 tình trạng huyết áp cao và huyết áp thấp đều gây nguy hiểm tới sức khỏe của người bệnh. Bất cứ ai trong số chúng ta cũng đều nên nắm rõ các chỉ số huyết áp để theo dõi huyết áp của mình nằm trong vùng nào để điều chỉnh chế độ ăn uống và lối sống cho phù hợp.<br>
<br>
<ul>
    <li>Huyết áp bình thường: Đối với người trưởng thành, khi các chỉ số huyết áp tâm thu dưới 120mmHg và huyết áp tâm trương dưới 80mmHg thì được gọi là huyết áp bình thường.</li>
    <li>Huyết áp cao : Khi chỉ số huyết áp tâm thu lơn hơn 140 mmHG và huyết áp tâm trương lớn hơn 90 mmHg thì được chẩn đoán là huyết áp cao.</li>
    <li>Tiền cao huyết áp là mức giá trị của các chỉ số huyết áp nằm giữa huyết áp bình thường và cao huyết áp (Huyết áp tâm thu từ 120-139 mmHg hoặc huyết áp tâm trương từ 80-89 mmHg)</li>
    <li>Huyết áp thấp: Hạ huyết áp (huyết áp thấp) được chẩn đoán khi huyết áp tâm thu dưới 90 mmHg hoặc giảm 25 mmHg so với bình thường.</li>
    <li>Để kết luận một người bị tăng huyết áp hay không người ta cần căn cứ vào trị số huyết áp của nhiều ngày. Đo đó phải đo huyết áp thường xuyên, nhiều lần trong ngày, theo dõi trong nhiều ngày. Phải đo huyết áp cả hai tay sau 5 phút nằm nghỉ và sau tối thiểu 1 phút ở tư thế đứng. Ở một số người huyết áp có thể tăng nhất thời khi quá xúc cảm, stress, hoặc sau khi uống rượu, bia, sau tập luyện, lao động nặng… chẳng hạn.</li>
</ul>
<br>
                Huyết áp cao và huyết áp thấp có nguy hiểm không?<br>
<br>
                Không ai phủ nhận mức độ nguy hiểm của huyết áp cao và huyết áp thấp. Người ta coi đây là các sát thủ thầm lặng với sức khỏe con người bởi diễn biến âm thầm, các triệu chứng không rõ ràng và các biến chứng nguy hiểm mà nó để lại.<br>
                do huyet ap Huyết áp là gì ?<br>
<br>
<p class="text-center"><img src="/img/article/1do-huyet-ap.jpg"></p>
                <p class="text-center">Đo huyết áp thường xuyên là biện pháp tối ưu kiểm soát huyết áp của bạn và gia đình<br></p>
<br>
                Huyết áp cao nguy hiểm như thế nào?<br>
<br>
                Nếu như huyết áp cao là bệnh thuờng gặp và gia tăng theo tuổi, là nguyên nhân gây tử vong và di chứng thần kinh nặng nề như liệt nửa người, hôn mê với đời sống thực vật, đồng thời có thể thúc đẩy suy tim, thiếu máu cơ tim làm ảnh hưởng nhiều đến chất lượng sống (không cảm thấy khoẻ khoắn, mất khả năng lao động) và gia tăng khả năng tử vong. Huyết áp cao còn là một trong những yếu tố nguy cơ của đột quỵ, nhồi máu cơ tim, suy tim và phình động mạch, ngoài ra nó còn là nguyên nhân gây ra suy thận mãn và biến chứng ở mắt. Tăng áp lực máu động mạch sẽ dẫn tới giảm tuổi thọ trung bình.<br>
<br>
                Nếu không được điều trị kịp thời, huyết áp cao có thể gây nhiều biến chứng nguy hiểm cho sức khỏe:<br>
<br>
<ul>
    <li>Biến chứng tức thời: Có thể nguy hiểm đến tính mạng, gồm tai biến mạch máu não, nhồi máu cơ tim cấp, bóc tách động mạch chủ, phù phổi cấp, suy thận cấp.</li>
    <li>Biến chứng lâu dài: Xảy ra nếu bệnh nhân sau một thời gian dài tăng huyết áp mà không được chẩn đoán và điều trị đúng. Biến chứng gồm: Rối loạn tiền đình, bệnh lý mắt, tim to, suy tim, đau thắt ngực do thiếu máu cục bộ cơ tim, suy thận mạn, đau cách hồi.</li>
</ul>
                <br>
                Huyết áp thấp nguy hiểm như thế nào?<br>
<br>
                Nếu so sánh với huyết áp cao, huyết ấp thấp trước mắt không dẫn đến biến chứng như tai biến mạch máu não, nghẽn tắc cơ tim nên nhiều người chủ quan với căn bệnh này. Tuy nhiên, ít người biết được rằng huyết áp thấp cũng có thể gây ra các biến chứng nguy hiểm không kém.<br>
<br>
                Khi người bệnh bị tụt huyết áp nhiều lần, hệ thống thần kinh bị suy giảm chức năng, cơ thể không tự kịp điều chỉnh để cung cấp đủ dinh dưỡng và oxy cho các cơ quan có chức năng sống còn như não, tim, thận gây ra các tổn thương cho các cơ quan này.<br>
<br>
                Nếu không được điều trị kịp thời, bệnh huyết áp thấp có thể dẫn đến tình trạng đau thắt ngực, nhồi máu cơ tim, suy thận… thậm chí nguy hiểm đến tính mạng. Nhiều trường hợp huyết áp thấp có thể dẫn tới tai biến mạch máu não, trong đó phần lớn là nhồi máu não, tỷ lệ này chiếm khoảng 30%. Ngoài ra, người bị tụt huyết áp cấp có thể gây sốc, đặc biệt nguy hiểm đến tính mạng trong những trường hợp như đang lái xe, làm việc trên tầng cao… Nếu huyết áp thấp kéo dài, còn làm cho các cơ quan thận, gan, tim, phổi suy yếu nhanh chóng.<br>
            <br>
        </div>
    </div>
    <div class="col-sm-4 margin-t">
        <div class="alert alert-success text-center margin-b">
            <span class="size-h4">
                The first 100 pre-orders get <b>FREE</b> Tele-Blood Pressure Monitor!
                <br>
                Only <b>20</b> remains! <br>
                <a href="/#pricing">PRE-ORDER now!</a>
            </span>
        </div>
<!--        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                Khuyen mai
            </div>
            <div class="panel-body">
        
            </div>
        </div>-->
        <div class="panel panel-default margin-b">
            <div class="panel-heading">
                Cam nang suc khoe
            </div>
            <div class="panel-body">
                <ul class="nav nav-list">
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                    <li><a>Article Title</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>