<div class="container margin-t">
    <div class="alert alert-default text-center">
        <p class="size-h3">Order successful!</p>
        Thank you for choosing HelloHealth. We will contact you soon!
    </div>
</div>