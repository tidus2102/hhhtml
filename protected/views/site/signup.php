<div class="intro-bg">
    <div class="intro-block">
        <div class="container">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
    <!--                    <span class="box-icon">
                            <i class="fa fa-h-square fa-lg"></i>
                        </span>-->
                        <span class="size-h4">Wecome to HelloHealth</span>
                    </div>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal" id="register-form">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control left-icon" placeholder="Full Name">
                                    <i class="fa fa-user left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control left-icon" placeholder="Email">
                                    <i class="fa fa-envelope left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="password" class="form-control left-icon" placeholder="Password">
                                    <i class="fa fa-lock left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-info btn-block" type="submit">SIGN UP</button>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                If you are doctor, <a class="" href="/signup-doctor">Sign up here</a>!
                            </div>
                            <div class="form-group text-center">
                                Already a member? <a href="/login" class="">Sign in now</a>
                            </div>
                        </form>
<!--                        <form role="form" class="" id="register-form">
                            <div class="form-group">
                                <div class="input-group col-sm-12">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" placeholder="Full Name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-sm-12">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" placeholder="Email or Phone" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group col-sm-12">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" placeholder="Password" >
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info btn-block" type="submit">SIGN UP</button>
                            </div>
                            <div class="form-group text-center">
                                If you are doctor, please <a class="" href="/signup-doctor">Sign up here</a>!
                            </div>
                            <div class="form-group text-center">
                                Already a member? <a href="/" class="">Sign in now</a>
                            </div>
                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
