<section class="section9" id="about">
    <div class="container text-center">
        <h1>ABOUT</h1>
        <!--<h2>HELLOHEALTH - SMARTER HEALTHCARE</h2>-->
        <div class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">
            <h4>HelloHealth is a the first mobile healthcare solution in Vietnam. Our mission is to empower patients and doctors, thereby improving the quality of healthcare services and public wellbeing.</h4>
<!--            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//www.youtube.com/embed/uYrJayGlaa8" allowfullscreen></iframe>
            </div>-->
            <a class="btn btn-play"></a>
        </div>
        <!--<div class="margin-t">
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                         <i class="icon-heart"></i>
                    </span>
                    <h3>Assurance</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="icon-glyph-5"></i>
                    </span>
                    <h3>Professional</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="icon-users"></i>
                    </span>
                    <h3>Care</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                </div>
            </div>
        </div>-->
    </div>
</section>

<section class="section4" id="service">
    <div class="container">
        <div class="text-center">
            <h1>SERVICES</h1>
            <div class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">
                <h4>Our health management system will help you connect with your family & your doctor more easily.</h4>
            </div>
        </div>
        <div class="margin-t col-sm-12">
            <img src="/img/company/iMac.png" class="img-responsive" style="width: 1000px;">
        </div>
        <div class="margin-t">
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                         <i class="icon-glyph-2"></i>
                    </span>
                    <h3>Track health online</h3>
                    <p>Our system help you track & check your health data anytime, anywhere</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="fa fa-user-md"></i>
                    </span>
                    <h3>Add your doctor</h3>
                    <p>Your personal doctor can easily track your health status & discussion with you</p>
                </div>
            </div>

            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="fa fa-mobile-phone"></i>
                        <!--<i class="glyphicon glyphicon-picture"></i>-->
                    </span>
                    <h3>Get alert via SMS</h3>
                    <p>Your family & your doctor will receive alert SMS if your health record is out of safe zone</p>
                </div>
            </div>
            <!--
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="fa fa-group"></i>
                    </span>
                    <h3>Add guardian</h3>
                    <p>Your family can follow your health with you easily</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="fa fa-comments"></i>
                    </span>
                    <h3>Discussion with doctor</h3>
                    <p>Free to discuss with your doctor about your health</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4 service-box">
                <div class="text-center">
                    <span class="box-icon">
                        <i class="icon-glyph-3"></i>
                    </span>
                    <h3>Healthcare consulting</h3>
                    <p>Weekly phone consulting with all experienced doctors</p>
                </div>
            </div>-->
        </div>
        
        <!--        <div class="margin-t margin-b">
            <img src="/img/company/s4service_en.png" class="img-responsive">
        </div>-->
<!--        <div class="margin-t margin-b">
            <div class="service-item">
                <div class="col-sm-6">
                    <div class="service-content">
                        <h3>Track health record online</h3>
                        <p>Our system store your blood pressure data so you check your health record online any time, any where.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="/img/company/0service1.png" class="img-responsive">
                </div>
            </div>
            <div class="service-item">
                <div class="col-sm-6">
                    <img src="/img/company/0service2.png" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <div class="service-content">
                        <h3>Add doctor & guardian</h3>
                        <p>You can request a doctor on HelloHealth system become your main doctor. He/She will track your health record, discuss with you about your health and receive alert when your health status has problem. You can add your family, your friends or even your private doctors as your guardians so they can follow your health with you too.</p>
                    </div>
                </div>
            </div>
            <div class="service-item">
                <div class="col-sm-6">
                    <div class="service-content">
                        <h3>Get alert via Email, SMS</h3>
                        <p>When your health record is out of safe zone, you, your main doctor & your guardian will receive alert via Email or SMS.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="/img/company/0service3.png" class="img-responsive">
                </div>
            </div>
        </div>-->
    </div>
</section>

<section class="section5" id="pricing">
    <div class="container">
        <h1 class="text-center">PLAN</h1>
<!--        <div class="alert alert-success text-center margin-t">
            <span class="size-h4">
                The first 100 pre-orders get <b>FREE</b> Tele-Blood Pressure Monitor!
                <br>
                Only <b>20</b> remains! CHOOSE now!
            </span>
        </div>-->
        <div class="pricing-table text-center">
            <div class="col-sm-4 col-xs-4">
                <div class="pricing-body pricing-option1">
                    <div class="pricing-title">
                        <h2>Basic</h2>
<!--                        <i class="fa fa-heart-o fa-2x"></i>-->
                    </div>
                    <ul>
                        <!--<li>Get daily health info</li>-->
                        <li>Track health record online</li>
                        <li>Add guardian</li>
                        <li>Receive alert via email & app</li>
                        <li>General QA with doctor</li>
                        <li>---</li>
                        <li>---</li>
                        <li>---</li>
<!--                                <li class="more-account">
                            + $49 / account / year
                            <div><small class="text-muted">Maxinum 4 accounts</small></div>
                        </li>-->
                    </ul>
                    <div class="pricing-price">
                        <!--<h2><sup>$</sup>0</h2>-->
                        <h2>FREE</h2>
                    </div>
                    <div class="pricing-action">

                        <a href="/signup" class="btn">SIGN UP</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <div class="pricing-body pricing-option2">
                    <div class="pricing-title">
<!--                            <div class="pt-ribbon-wrapper">
                            <div class="pt-ribbon">
                                Best
                            </div>
                        </div>-->
                        <h2>Standard</h2>
<!--                        <i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i>-->
                    </div>
                    <ul>
                        <!--<li>Get daily health info</li>-->
                        <li>Track health record online</li>
                        <li>Add guardian</li>
                        <li>Receive alert via email & app</li>
                        <li>General QA with doctor</li>
                        <li>Add personal doctor</li>
                        <li>---</li>
                        <li>---</li>
<!--                                <li class="more-account">
                            + $89 / account / year
                            <div><small class="text-muted">Maxinum 4 accounts</small></div>
                        </li>-->
                    </ul>
                    <div class="pricing-price">
                        <h2>
<!--                            <sup>$</sup>299-->
                            Trial
                        </h2>
<!--                        <span class="text-muted">Yearly</span>-->
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn">Comming Soon</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-4">
                <div class="pricing-body pricing-option3">
                    <div class="pricing-title">
                        <h2>Premium</h2>
<!--                        <i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i><i class="fa fa-heart-o fa-2x"></i>-->
                    </div>
                    <ul>
<!--                        <li>Get daily health info</li>-->
                        <li>Track health record online</li>
                        <li>Add guardian</li>
                        <li>Receive alert via email & app</li>
                        <li>General QA with doctor</li>
                        <li>Choose personal doctor</li>
                        <li>Receive alert via SMS</li>
                        <li>Health-check booking</li>
<!--                                <li class="more-account">
                            + $49 / account / year
                            <div><small class="text-muted">Maxinum 4 accounts</small></div>
                        </li>-->
                    </ul>
                    <div class="pricing-price">
                        <h2>
                            <!--                            <sup>$</sup>299-->
                            Trial
                        </h2>
                        <!--                        <span class="text-muted">Yearly</span>-->
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="btn">Comming soon</a>
                    </div>
                </div>
            </div>
<!--            <div class="col-sm-3 col-xs-3">
                <div class="pricing-plan">
                    <div class="pricing-title">
                        <h3>Choose your plan</h3>
                    </div>
                    <ul>
                        <li>Get daily health info <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>General QA with doctor <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>Track health record online <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="HeloHealth system help you track your health record from your device"></i></li>
                        <li>Add guardian <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You can add your family or your private doctor so they follow your health too"></i></li>
                        <li>Receive alert via email <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You & your guardian will receive alert via email when your health record have problem"></i></li>
                        <li>Get mobile app <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>Choose personal doctor <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You can choose your doctor from doctor list on the HelloHealth system. He/She will follow your health record & give you feedback for your health status"></i></li>
                        <li>Receive alert via SMS <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You, your doctor & your guardian will receive alert via SMS when your health have problem"></i></li>
                        <li>Healthcare phone consulting <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>Health-check booking <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>

                        <li>Subscribe healthcare news <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>Join community <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>View hopital, clinic list <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                        <li>View doctor list <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You can view all doctor profile on HelloHealth system"></i></li>
                        <li>Discussion with doctor <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title="You can discuss with your main doctor about your health"></i></li>
                        <li>Get reminder from doctor note <i class="fa fa-info-circle" rel="tooltip" data-placement="top" title=""></i></li>
                    </ul>
                </div>
            </div>-->
<!--            <div class="col-sm-9 col-xs-9 pricing-option">-->

<!--            </div>-->
        </div>
    </div>
</section>

<section class="section7" id="doctor">
    <div class="container">
        <div class="text-center">
            <h1>DOCTOR</h1>
            <!--<h2>JOIN HEALTHCARE NETWORK</h2>-->
        </div>
<!--        <div class="doctor-list text-center">
            <div class="col-sm-12">
                <a href=""><img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle"></a>
                <a href=""><img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle"></a>
                <a href=""><img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle"></a>
                <a href=""><img src="/img/sample/doctor.jpg" class="img-thumbnail img-circle"></a>
            </div>
            <div class="text-center">
                <a href="/doctor">View All Doctors</a>
            </div>
            <div class="clearfix"></div>
        </div>-->
        <div class="doctor-content">
            <div class="col-sm-5 doctor-profile text-center">
                <ul class="bxslider" id="doctorslider">
                    <li class="bxslide">
                        <img src="/img/company/doctor_en.jpg" class="img-circle img-thumbnail img-responsive">
                        <div class="doctor-profile-info">
                            <h3>Dr. Nguyễn Ngọc Lân</h3>
                            <small>Chuyên khoa tim mạch bệnh viện Y Dược</small>
                        </div>
                    </li>
                    <li class="bxslide">
                        <img src="/img/company/doctor_en.jpg" class="img-circle img-thumbnail img-responsive">
                        <div class="doctor-profile-info">
                            <h3>Dr. Đồng Quang Tráng</h3>
                            <small>Chuyên khoa tim mạch bệnh viện Y Dược</small>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-sm-7">
                <div class="margin-t">
                    <div class="doctor-icon">
                        <span class="box-icon bg-primary">
                             <i class="fa fa-users"></i>
                        </span>
                    </div>
                    <div class="doctor-info">
                        <h4>Healthcare Network</h4>
                        <p>Our network help you connect with patients & other doctors easily.</p>
                    </div>
                </div>
                <div class="margin-t">
                    <div class="doctor-icon">
                        <span class="box-icon bg-primary">
                            <i class="fa fa-bar-chart-o"></i>
                        </span>
                    </div>
                    <div class="doctor-info">
                        <h4>Patient Management</h4>
                        <p>You can easily track & manage your patients' status anytime, anywhere.</p>
                    </div>
                </div>
                <div class="margin-t">
                    <div class="doctor-icon">
                        <span class="box-icon bg-primary">
                             <i class="fa fa-dollar"></i>
                        </span>
                    </div>
                    <div class="doctor-info">
                        <h4>Financial Benefits</h4>
                        <p>HelloHealth offers you the solution to improve your net collections.</p>
                    </div>
                </div>
            </div>
            <div class="margin-t text-center col-sm-12 btn-action">
<!--                <a class="btn btn-primary" href="/doctor" target="_blank">OUR DOCTORS</a>-->
                <a class="btn btn-info" href="/signup-doctor">JOIN US</a>
            </div>
        </div>
    </div>
</section>


<section class="section10" id="health">
    <div class="container">
        <h1 class="text-center">NEWS</h1>
        
        <div class="article panel panel-default margin-t margin-b article-list">
            <div class="panel-heading">
                <a href="/article">HUYẾT ÁP - TIM MẠCH</a>
                <a class="pull-right" href="/artlce">Xem tất cả</a>
            </div>
            <div class="panel-body">
                <div class="col-sm-3 col-xs-6 article">
                    <a href=""><img src="/img/article/1do-huyet-ap-190x130.jpg" class=""></a>
                    <div class="article-content">
                        <small><i class="fa fa-clock-o"></i> Oct 10, 2014</small>
                        <a href="/articleDetails"><h5>Huyết áp là gì?</h5></a>
                        <p>Có 2 vấn đề sức khỏe đang gây nhức nhối với cộng đồng đó là huyết áp cao và huyết áp thấp</p>
                        <a href="/articleDetails" class="btn btn-primary">Đọc thêm</a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 article">
                    <a href=""><img src="/img/article/2816487-190x130.jpeg" class=""></a>
                    <div class="article-content">
                        <small><i class="fa fa-clock-o"></i> Oct 10, 2014</small>
                        <a href="/articleDetails"><h5>Nhận biết sớm các triệu chứng huyết áp cao</h5></a>
                        <p>Huyết áp cao vô cùng nguy hiểm bởi nó có thể gây ra những ảnh hưởng nghiêm trọng tới sức khỏe</p>
                        <a href="/articleDetails" class="btn btn-primary">Đọc thêm</a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 article">
                    <a href=""><img src="/img/article/1do-huyet-ap-190x130.jpg" class=""></a>
                    <div class="article-content">
                        <small><i class="fa fa-clock-o"></i> Oct 10, 2014</small>
                        <a href="/articleDetails"><h5>Huyết áp là gì?</h5></a>
                        <p>Có 2 vấn đề sức khỏe đang gây nhức nhối với cộng đồng đó là huyết áp cao và huyết áp thấp.</p>
                        <a href="/articleDetails" class="btn btn-primary">Đọc thêm</a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6 article">
                    <a href=""><img src="/img/article/2816487-190x130.jpeg" class=""></a>
                    <div class="article-content">
                        <small><i class="fa fa-clock-o"></i> Oct 10, 2014</small>
                        <a href="/articleDetails"><h5>Nhận biết sớm các triệu chứng huyết áp cao</h5></a>
                        <p>Huyết áp cao vô cùng nguy hiểm bởi nó có thể gây ra những ảnh hưởng nghiêm trọng tới sức khỏe</p>
                        <a href="/articleDetails" class="btn btn-primary">Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="panel panel-default margin-t margin-b article-list">
            <div class="panel-heading">
                <a href="/article">TIỂU ĐƯỜNG</a>
            </div>
            <div class="panel-body">
                <div class="col-sm-6">
                    <a href=""><img src="/img/article/1do-huyet-ap-190x130.jpg" class="img-thumbnail"></a>
                    <p><a href="/articleDetails">Huyết áp là gì?</a></p>
                    <p class="article-content">
                        Có 2 vấn đề sức khỏe đang gây nhức nhối với cộng đồng đó là huyết áp cao và huyết áp thấp. Vậy huyết áp là gì?<br>
                        <a href="/articleDetails">Doc them</a>
                    </p>
                </div>
                <div class="col-sm-6">
                    <a href=""><img src="/img/article/2816487-190x130.jpeg" class="img-thumbnail"></a>
                    <p><a href="/articleDetails">Nhận biết sớm các triệu chứng huyết áp cao</a></p>
                    <p class="article-content">
                        Huyết áp cao vô cùng nguy hiểm bởi nó có thể gây ra những ảnh hưởng nghiêm trọng tới sức khỏe và chất lượng cuộc sống <br>
                        <a href="/articleDetails">Doc them</a>
                    </p>
                </div>
            </div>
        </div>-->
        
        <div class="col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10 text-center margin-t margin-b text-center">
            <h4>Đăng ký để nhận thông tin về y tế sức khỏe miễn phí</h4>
            <form method="post" action="" id="" class="">
                <div class="form-inline">
                    <input type="text" placeholder="Name" class="form-control">
                    <input type="text" placeholder="Email" class="form-control">
<!--                    <span class="input-group-btn">-->
                        <button type="submit" class="btn btn-primary ">Đăng ký</button>
<!--                    </span>-->
                </div>
<!--                <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Đăng ký</button>
                    </span>
                </div>-->
            </form>
        </div>
<!--        <div class="text-center">
            <a href="/artlce">Read all</a>
        </div>-->
    </div>
</section>

<section class="section8" id="contact">
<!--    <div class="subscribe">
        <div class="container ">
            <div class="col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10 text-center subscribe-form">
                <form method="post" action="/subscribe/create2" id="subscribe-form" class="">
                    <div class="input-group">
                        <input type="text" placeholder="Email" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Subscribe</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>-->
    <div id="map" class="google-maps ">
    </div>
</section>

<header class="navbar-fixed-top" style="display: none;" id="fixtopmenu"> 
    <div class="container">
        <div class="">
            <a href="#home" class="navbar-brand scroll"><img src="/img/logocolor.png"></a>
        </div>
        <nav role="navigation" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="/">Home</a></li>
                <!-- <li><a href="#product" class="scroll">Product</a></li> -->
                <li><a href="#about" class="scroll">About</a></li>
                <li><a href="#service" class="scroll">Services</a></li>
                <li><a href="#pricing" class="scroll">Plan</a></li>
                <li><a href="#doctor" class="scroll">Doctor</a></li>
<!--                <li><a href="#health" class="scroll">News</a></li>-->
                <li><a href="#contact" class="scroll">Contact</a></li>
            </ul>
        </nav>
    </div>
</header>

    <script>
//    $(document).ready(
//        function() {
//            $('.smoothScroll').smoothScroll();
//        }
//    );
//    $('#fixtopmenu').headroom();
    
    $(document).ready(function() {
        $('#homeslider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: false,
            speed: 2000,
//            captions: true
//            controls: false;
        });
        
        $('#doctorslider').bxSlider({
            mode: 'fade',
            controls: false,
            pager: false,
//            speed: 3000,
            auto: true,
//            captions: true
        });
        
        //alert($(window).width() + " - " + $(window).height());
        function setHeight() {
            //var windowHeight = $(window).height() - 40;
            var windowHeight = $(window).height();
//            $('.section2').css({ 
//                'height': windowHeight + 'px'
//            });
            $("#home div:nth-child(2) div").first().css({ 
                'height': windowHeight + 'px'
            });
            $('.homeslide').css({ 
                'height': windowHeight + 'px'
            });
        }

        $(window).on('resize', function() { setHeight(); });
        setHeight();
  
        $("[rel='tooltip']").tooltip(); 
        $("[rel='popover']").popover({
            trigger: 'hover'
        }); 
//        $('textarea').autosize();
        
//        $('.perfectScrollbar').perfectScrollbar({
//            wheelSpeed: 40
//        });
        
        var width = $(window).width();
        if (width < 750) {
            //No fixed-top menu
            $("header").removeClass('navbar-fixed-top');
            
            //Push Right Menu
            var body = $('#body'), menu = $('#cbp-spmenu-s2'), showRightPush = $('#showRightPush');
            $(document).on('click', '*', function(e) {
                e.stopPropagation(); //<-- stop the event propagating to ancestral elements
                if ($(this).is(showRightPush))   //<-- on button click, toggle visibility of menu
                {
                    menu.toggleClass('cbp-spmenu-open');
                    body.toggleClass('cbp-spmenu-push-toleft');
                }
                else if (!$(this).closest(menu).length) //<-- on click outside, hide menu
                {
                    menu.removeClass('cbp-spmenu-open');
                    body.removeClass('cbp-spmenu-push-toleft');
                }
            });
        }
    
        $('#gototop').hide();
        
        $(window).scroll(function() {
            if ($(this).scrollTop() > $(window).height() - 100) {
                $('#fixtopmenu').show();
//                $('#fixtopmenu').addClass("active");
//                $('#fixtopmenu').addClass("navbar-fixed-top");
            } else {
                $('#fixtopmenu').hide();
//                $('#fixtopmenu').removeClass("active");
//                $('#fixtopmenu').removeClass("navbar-fixed-top");
            }

            if(this.scrollY > 700){
                $('#gototop').show();
            }else{
                $('#gototop').hide();
            }
        });
    
        //$('a[href*=#]:not([href=#])').click(function() {
        $('.scroll').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
    //                if ($(window).scrollTop() > target.offset().top)
    //                    $('html,body').animate({
    //                        scrollTop: target.offset().top - $('header').height()
    //                    }, 1000);
    //                else
    //                    $('html,body').animate({
    //                        scrollTop: target.offset().top 
    //                    }, 1000);
                    if ($(this).attr('href') == "#top")
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                    else
                        $('html,body').animate({
                            scrollTop: target.offset().top - $('header').height()
                        }, 1000);
                    return false;
                }
            }
        });
    });
    
    //Google map
    jQuery('#map').gMap({
        address: "88a Trần Huy Liệu, phường 15, Thành phố Hồ Chí Minh, quận Phú Nhuận, Hồ Chí Minh, Vietnam",
        zoom: 16,
        controls: {
//         panControl: true,
         zoomControl: true,
         mapTypeControl: true,
         scaleControl: true,
//         streetViewControl: true,
         overviewMapControl: true
     },
        markers: [{
            address: "88a Trần Huy Liệu, phường 15, Thành phố Hồ Chí Minh, quận Phú Nhuận, Hồ Chí Minh, Vietnam",
            html: "<div class='text-center'><img src='/img/logocolor.png'><p>Smarter Healthcare</p><p><span class='icon glyphicon glyphicon-map-marker'></span> 88A Trần Huy Liệu, P.15, Q.Phú Nhuận, Tp HCM</p><p><span class='icon glyphicon glyphicon-phone'></span> 1900.6.135</p><p><span class='icon glyphicon glyphicon-envelope'></span> <a href='mailto:info@hellohealth.vn'>info@hellohealth.vn</a></p></div>",
            popup: true
        }]
    });

//    $(function () {
//        var DP = $('.datepicker').datepicker({
//            format: 'dd-mm-yyyy'
//        }).on('changeDate', function(ev) {
//            DP.datepicker('hide');
//        });
//    });  
    
//    $(document).ready(
//        function() {
//            $('.items').perfectScrollbar({wheelSpeed: 40});
//        }
//    );

    
    
//    $(document).on('click', '.changeLanguage', function(event){
//        event.preventDefault();
//        
//        var lang = $(this).attr('lang');
//        
//        var url = '/site/changeLanguage';
//        $.post(url, {lang: lang}, function(data) {
//            if(data.success == true){
//                location.reload();
//            }
//        },'json');
//    });


</script>




<!--<div class="container">-->
<!--<h1 class="text-center">CONTACT</h1>-->
<!--            <div class="col-sm-7">
            <h4>HelloHealth</h4>
            <ul class="nav nav-list list-info margin-t">
                <li class="ng-binding">
                    <span class="icon glyphicon glyphicon-home"></span>
                    <label>Company</label>
                    HelloHealth JSC
                </li>
                <li class="ng-binding">
                    <span class="icon glyphicon glyphicon-map-marker"></span>
                    <label>Address</label>
                    88A Trần Huy Liệu, Phường 15, Quận Phú Nhuận, Tp Hồ Chí Minh
                </li>
                <li>
                    <span class="icon glyphicon glyphicon-phone"></span>
                    <label>Hotline</label>
                    1900.6.135
                </li>
                <li>
                    <span class="icon glyphicon glyphicon-envelope"></span>
                    <label>Email</label>
                    <a href="mailto:info@hellohealth.vn">info@hellohealth.vn</a>
                </li>
            </ul>
        </div>-->
<!--</div>-->

<?/*<section class="section3" id="product">
    <div class="container">
        <div class="text-center">
            <h1>PRODUCT</h1>
            <h2>TELE-BLOOD PRESSURE MONITOR</h2>
        </div>
<!--            <h3 class="text-center">
            The device checks your blood pressure and send it the HelloHealth system.
        </h3>-->
        <div class="margin-t product-info">
            <div class="col-sm-6 margin-t">
                <img src="/img/company/s3product.png" class="img-responsive">
            </div>
            <div class="col-sm-6 margin-t">
<!--                    <div class="product-feature">
                    <span class="box-icon">
                         <i class="fa fa-magic"></i>
                    </span>
                    <div class="feature-content">
                        <h3>Smart Design</h3>
                        <p>Clearly screen</p>
                    </div>
                </div>-->
                <div class="product-feature">
                    <span class="box-icon">
                         <i class="fa fa-magic"></i>
                    </span>
                    <div class="feature-content">
                        <h3>Smart Design</h3>
                        <p>Simple and easy to use. Press the START button to power on and press it again to record your blood pressure.</p>
                    </div>
                </div>
                <div class="product-feature">
                    <span class="box-icon">
                         <i class="fa fa-check"></i>
                    </span>
                    <div class="feature-content">
                        <h3>Verified Quality</h3>
                        <p>Accurate and stable. It has won Gold Award in International Techmart Vietnam 2012.</p>
                    </div>
                </div>
                <div class="product-feature">
                    <span class="box-icon">
                         <i class="fa fa-globe"></i>
                    </span>
                    <div class="feature-content">
                        <h3>High Mobility</h3>
                        <p>You can use it everywhere. It sends the data via GPRS.</p>
                    </div>
                </div>
            </div>
        </div>
<!--            <hr>
        <h3 class="text-center">
            The very first product of Hello Health - Tele-Blood Pressure Monitor
        </h3>
        <div class="margin-t">
            <div class="col-sm-3 col-xs-3">
                <div class="text-center">
                    <span class="box-icon">
                         <i class="fa fa-shield"></i>
                     </span>
                    <h4>Warranty</h4>
                    <p>The device has 3 years warranty</p>
                </div>
            </div>
        </div>-->
    </div>
</section>*/?>