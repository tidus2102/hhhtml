<div class="signup-block">
    <div class="container">
        <div class="col-sm-7">
            
<!--            <img src="/img/doctors.jpg">-->
        </div>
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
        <!--                    <span class="box-icon">
                        <i class="fa fa-h-square fa-lg"></i>
                    </span>-->
                    <span class="size-h4">Doctor Registeration</span>
                </div>
                <div class="panel-body">
                    <form role="form" class="">
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-lightbulb-o"></i></span>
                                <input type="text" class="form-control datepicker" placeholder="Birthday">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                <input type="text" class="form-control" placeholder="Identity Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" placeholder="Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" class="form-control" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" class="form-control" placeholder="Specialties">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-user-md"></i></span>
                                <input type="text" class="form-control" placeholder="Doctor License">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-sm-12">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-info btn-block" type="submit">SIGN UP</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>