<!--<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
            <a href=""><h3>Dr Trần Công Minh</h3></a>
            <ul class="nav nav-info">
                <li>Male, 50</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
    </div>
</div>-->
<!--<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/doctor/profile">Profile</a></li>
            <li><a href="/doctor/community2">Community</a></li>
        </ul>
    </div>
</div>-->
<div class="profile-info container margin-t">
    <div class="col-sm-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Doctor Information
            </div>
            <div class="panel-body">
                <div class="text-center">
                    <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
                    <h3>Dr Trần Công Minh</h3>
                    <span class="">Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch TP. Hồ Chí Minh</span>
                </div>
                <ul class="nav nav-list list-info margin-t">
<!--                    <li class="ng-binding">
                        <span class="icon glyphicon glyphicon-user"></span>
                        <label>Sex</label>
                        Trần Công Minh
                    </li>-->
                    <li>
                        <span class="icon glyphicon glyphicon-envelope"></span>
                        <label>Certifications</label>
                        Associate Professor, Dr.
                    </li>
                    <li>
                        <span class="icon glyphicon glyphicon-home"></span>
                        <label>Specialties</label>
                        Heart
                    </li>
                    <li>
                        <span class="icon glyphicon glyphicon-earphone"></span>
                        <label>Experience</label>
                        5 years
                    </li>
                    <li>
                        <span class="icon glyphicon glyphicon-earphone"></span>
                        <label>Work Places</label>
                        115 Hospital
                    </li>
                    <li>
                        <span class="icon glyphicon glyphicon-earphone"></span>
                        <label>Personal Clinic</label>
                        143/22 Nguyễn Thị Tần, P2, Q8
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Review
            </div>
            <div class="panel-body">
                <ul class="nav nav-list list-info">
                    <li>
                        <span class="icon glyphicon glyphicon-user"></span>
                        <label><a>User ABC</a></label>
                        Bác sĩ rất nhiệt tình!
                    </li>
                    <li>
                        <span class="icon glyphicon glyphicon-user"></span>
                        <label><a>User ABC</a></label>
                        Bác sĩ rất nhiệt tình!
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <iframe width="633" height="400" src="//www.youtube.com/embed/-dZC0Jz_kBM" frameborder="0" allowfullscreen></iframe>
        <div class="panel panel-default margin-t">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Working Experience
            </div>
            <div class="panel-body">
                <div class="feed-item">
                    <div class="feed-icon">
                        <span class="round-icon bg-info"><i class="fa fa-check"></i></span>
                    </div>
                    <div class="feed-subject">
                        <p>
                            <!--<a href="#">Dr Trần Công Minh</a>-->
                            <b>Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch HCM</b>
                            <span class="pull-right text-muted">2013 - Now</span>
                        </p>
                    </div>
                    <div class="feed-content">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                    </div>
                </div>
                <div class="feed-item">
                    <div class="feed-icon">
                        <span class="round-icon bg-info"><i class="fa fa-check"></i></span>
                    </div>
                    <div class="feed-subject">
                        <p>
                            <!--<a href="#">Dr Trần Công Minh</a>-->
                            <b>Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch HCM</b>
                            <span class="pull-right text-muted">2007 - 2013</span>
                        </p>
                    </div>
                    <div class="feed-content">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                    </div>
                </div>
                <div class="feed-item">
                    <div class="feed-icon">
                        <span class="round-icon bg-info"><i class="fa fa-check"></i></span>
                    </div>
                    <div class="feed-subject">
                        <p>
                            <!--<a href="#">Dr Trần Công Minh</a>-->
                            <b>Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch HCM</b>
                            <span class="pull-right text-muted">2000 - 2007</span>
                        </p>
                    </div>
                    <div class="feed-content">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>