<div class="container">
    <h1 class="text-center">Account</h1>
    <div class="margin-t">
        <ul class="nav nav-pills " data-tabs="tabs">
            <li><a data-toggle="tab" href="#s1"><i class="fa fa-user"></i> General</a></li>
            <li class="active"><a data-toggle="tab" href="#s2"><i class="fa fa-list-alt"></i> Profile</a></li>
            <li><a data-toggle="tab" href="#s3"><i class="fa fa-cog"></i> Setting</a></li>
        </ul>
    </div>
    <div class="tab-content ">
        <div id="s1" class="tab-pane">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="box-icon">
                        <i class="fa fa-user fa-lg"></i>
                    </span>
                    Information
                </div>  
                <div class="panel-body text-center">
                    <div class="col-sm-5">
                        <img src="/img/sample/patient.jpg" class="img-thumbnail img-circle avatar-large">
                        <div class="clearfix"></div>
                        <a class="btn btn-primary margin-t" href="">Change Avatar</a>
                    </div>
                    <div class="col-sm-7">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Birthday</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9 radio-inline">
                                    <label class="ui-radio"><input type="radio" name="gender" value="" checked="true"><span>Male</span></label>
                                    <label class="ui-radio"><input type="radio" name="gender" value=""><span>Female</span></label>
                                    <label class="ui-radio"><input type="radio" name="gender" value=""><span>Unknown</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Identity Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 pull-right text-right">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="s2" class="tab-pane active">
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                            <i class="fa fa-list-alt fa-lg"></i>
                        </span>
                        Information
                    </div>
                    <div class="panel-body">
<!--                        <div class="text-center">
                            <img class="img-thumbnail avatar-large img-circle" src="/img/sample/doctor.jpg">
                        </div>-->
                        <form role="form" class="">
                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <div class="">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Certification</label>
                                <div class="">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Specialties</label>
                                <div class="">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Experience</label>
                                <div class="">
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Work Places</label>
                                <div class="">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Personal Clinic</label>
                                <div class="">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <div class="">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                            <i class="fa fa-vimeo-square fa-lg"></i>
                        </span>
                        Video
                    </div>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <!--<label class="col-sm-3 control-label">Name</label>-->
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Video URL (Youtube, Vimeo)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 pull-right text-right">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                            <i class="fa fa-bars fa-lg"></i>
                        </span>
                        Working Experience
                    </div>
                    <div class="panel-body">
                        <div class="text-right">
                            <a id="btn-add-experience" class="btn btn-primary">Add Experience</a>
                        </div>
                        <form id="form-experience" role="form" class="form-horizontal margin-t">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Time</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" placeholder="From (Begin Time)">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" placeholder="To (End Time)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Position/Job Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Work Places</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Details</label>
                                <div class="col-sm-9">
                                    <textarea rows="3" class="form-control" placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 pull-right text-right">
                                    <input type="submit" class="btn btn-primary" value="Add">
                                </div>
                            </div>
                        </form>
                        <div class="margin-t">
                            <div class="feed-item">
                                <div class="feed-icon">
                                    <span class="round-icon bg-info"><i class="fa fa-check"></i></span>
                                    <!--<a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>-->
                                </div>
                                <div class="feed-subject">
                                    <p>
                                        <!--<a href="#">Dr Trần Công Minh</a>-->
                                        <b>Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch HCM</b>
                                        <span class="pull-right text-muted">2013 - Now</span>
                                    </p>
                                </div>
                                <div class="feed-content">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                </div>
                                <div class="btn-edit pull-right">
                                    <a href="">Edit</a> | <a href="">Delete</a>
                                </div>
                            </div>
                            <div class="feed-item">
                                <div class="feed-icon">
                                    <span class="round-icon bg-info"><i class="fa fa-check"></i></span>
                                </div>
                                <div class="feed-subject">
                                    <p>
                                        <!--<a href="#">Dr Trần Công Minh</a>-->
                                        <b>Tổng thư ký Hội Phẫu thuật Lồng ngực và Tim mạch HCM</b>
                                        <span class="pull-right text-muted">2013 - Now</span>
                                    </p>
                                </div>
                                <div class="feed-content">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                </div>
                                <div class="btn-edit pull-right">
                                    <a href="">Edit</a> | <a href="">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="s3" class="tab-pane">
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                            <i class="fa fa-lock fa-lg"></i>
                        </span>
                        Password
                    </div>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Current</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">New</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Confirm</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <div class="col-sm-3">
                                    <input type="submit" class="btn btn-primary" value="Change Password">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="box-icon">
                            <i class="fa fa-lock fa-lg"></i>
                        </span>
                        Setting
                    </div>
                    <div class="panel-body">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-5 control-label switch-label">Receive notifications via email</label>
                                <div class="col-sm-7 radio-inline">
                                    <!--<label class="ui-checkbox"><input type="checkbox" name="" value="" checked="true"><span>Receive notifications via Email</span></label>-->
                                    <label class="switch"><input type="checkbox" unchecked="true"><i></i></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label switch-label">Receive notifications via SMS</label>
                                <div class="col-sm-7 radio-inline">
                                    <label class="switch"><input type="checkbox" checked="true"><i></i></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label switch-label">Receive news via email</label>
                                <div class="col-sm-7 radio-inline">
                                    <label class="switch"><input type="checkbox" checked="true"><i></i></label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add User</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" placeholder="Type name...">
                        </div>
                    </div>
                </form>
                <table class="table table-hover user-list">
                    <tbody>
						<tr>
							<td>
								<a href=""><img class="img-thumbnail" src="/img/sample/user1.jpg"></a>
								<a href="">Trần Thanh Thúy</a>
<!--                                    <ul class="nav nav-list">
									<li>Specialties: Heart</li>
									<li>Experience: 15</li>
								</ul>-->
							</td>          
							<td class="text-center">
								<a href="" class="btn btn-primary">Add</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href=""><img class="img-thumbnail" src="/img/sample/user2.jpg"></a>
								<a href="">Lê Huy</a>
<!--                                    <ul class="nav nav-list">
									<li>Specialties: Heart</li>
									<li>Experience: 15</li>
								</ul>-->
							</td>          
							<td class="text-center">
								<a href="" class="btn btn-primary">Add</a>
							</td>
						</tr>
					</tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $("#form-experience").hide();
//        $(".show_hide").show();

        $('#btn-add-experience').click(function(){
//            event.preventDefault();
            $("#form-experience").toggle();
//            $("#form-experience").slideToggle();
        });
    });
</script>