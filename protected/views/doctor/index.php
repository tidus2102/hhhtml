<div class="info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
            <a href=""><h3>Dr Trần Công Minh</h3></a>
            <ul class="nav nav-info">
                <li>Male, 50</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 ">
            <a href="/doctor/profile" class="btn btn-primary pull-right">Doctor Profile</a>
        </div>
    </div>
</div>
<div class="tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/doctor/index">Dashboard</a></li>
            <li><a href="/doctor/patient">Patient</a></li>
            <!--<li><a href="/doctor/community">Community</a></li>-->
        </ul>
    </div>
</div>
<div class="container">
    <!--<div class="col-sm-5">-->
        <div class="panel panel-danger">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Warning
            </div>
            <div class="panel-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Date</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control datepicker">
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-hover user-list">
                        <thead>
                            <tr>
                                <th>Patient</th>
                                <th>Number Of Out Range</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail img-circle" src="/img/sample/patient.jpg"></a>
                                    <a href="">Nguyễn Huy Tuấn</a>
                                </td>
                                <td><a href="">3</a></td>
                                <td><span class="label label-warning">Hypertension</span> <span class="label label-danger">Hypertension Emergency</span></td>
                                <td><a class="btn btn-primary" data-toggle="modal" data-target="#patientModal1">View</a></td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail img-circle" src="/img/sample/user1.jpg"></a>
                                    <a href="">Trần Thanh Thúy</a>
                                </td>
                                <td><a href="">2</a></td>
                                <td><span class="label label-warning">Hypertension</span></td>
                                <td><a class="btn btn-primary" data-toggle="modal" data-target="#patientModal2">View</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?/*<div class="health-block">
                    <ul class="nav nav-pills nav-justified" data-tabs="tabs">
                        <li class="active"><a data-toggle="tab" href="#s1">Hypertension Emergency</a></li>
                        <li><a data-toggle="tab" href="#s2">Hypertension</a></li>
                        <li><a data-toggle="tab" href="#s3">Hypotension</a></li>
                    </ul>
                </div>-->
                <div class="tab-content content">
                    <div id="s1" class="tab-pane active">
                        <div id="accordion1" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient1" data-parent="#accordion1" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/patient.jpg">
                                            Nguyễn Tuấn Huy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient1">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <!--<th>Patient</th>-->
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                                            <a href="">Nguyễn Tuấn Huy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user1.jpg"></a>
                                                            <a href="">Trần Thanh Thúy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient2" data-parent="#accordion1" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/user1.jpg">
                                            Trần Thanh Thúy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient2">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="s2" class="tab-pane">
                        <div id="accordion2" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient21" data-parent="#accordion2" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/patient.jpg">
                                            Nguyễn Tuấn Huy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient21">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <!--<th>Patient</th>-->
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                                            <a href="">Nguyễn Tuấn Huy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user1.jpg"></a>
                                                            <a href="">Trần Thanh Thúy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient22" data-parent="#accordion2" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/user1.jpg">
                                            Trần Thanh Thúy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient22">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="s3" class="tab-pane">
                        <div id="accordion3" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient31" data-parent="#accordion3" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/patient.jpg">
                                            Nguyễn Tuấn Huy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient31">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <!--<th>Patient</th>-->
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                                            <a href="">Nguyễn Tuấn Huy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
            <!--                                            <td class="text-left">
                                                            <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user1.jpg"></a>
                                                            <a href="">Trần Thanh Thúy</a>
                                                        </td>-->
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#patient32" data-parent="#accordion3" data-toggle="collapse" class="collapsed">
                                            <img class="img-thumbnail" src="/img/sample/user1.jpg">
                                            Trần Thanh Thúy
                                            <!--<span class="badge">3</span>-->
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="patient32">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover user-list">
                                                <thead >
                                                    <tr>
                                                        <th>Blood Pressure Record</th>
                                                        <th>Record Time</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>16:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><span class="label label-danger">201</span><span class="label label-warning">111</span><span class="label label-info">76</span></td>
                                                        <td>10:00</td>
                                                        <td><a href="" class="btn btn-primary">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>*/?>
            </div>
        </div>
    <!--</div>-->
    <div class="patient-data">
        <div class="modal fade" id="patientModal1" tabindex="-1" role="dialog" aria-labelledby="patientModal1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nguyễn Huy Tuấn's Blood Pressure Record</h4>
                    </div>
                    <div class="modal-body">
                        <img src="/img/sample/linechart2.jpg" width="100%">
                        <div class="action-box margin-t">
                            <textarea placeholder="Create your topic..." rows="3" class="form-control"></textarea>
                            <div class="action-list">
                                <div class="action-item">
                                    <a title="" class="fa fa-picture-o" rel="tooltip" data-original-title="Post an Image"><i></i></a>
                                    <a title="" class="fa fa-video-camera" rel="tooltip" data-original-title="Upload a Video"><i></i></a>
                <!--                    <a title="" class="fa fa-lightbulb-o" rel="tooltip" data-original-title="Post an Idea"><i></i></a>
                                    <a title="" class="fa fa-question-circle" rel="tooltip" data-original-title="Ask a Question"><i></i></a>-->
                                </div>	
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm">Post</a>
                                </div>
                            </div>
                        </div>
                        <div class="margin-b">
                            <div class="feed-item">
                                <div class="feed-icon">
                                    <!--<i class="fa fa-lightbulb-o"></i>-->
                                    <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                                </div>
                                <div class="feed-subject">
                                    <p>
                                        <a href="#">Dr Trần Công Minh</a>
                                        <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                    </p>
                                </div>
                                <div class="feed-content">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                </div>
                                <div class="feed-actions">
                    <!--                <a class="pull-left" rel="tooltip"><i class="fa fa-thumbs-up"></i> 123</a> 
                                    <a class="pull-left" rel="tooltip"><i class="fa fa-comment-o"></i> 29</a>-->
                                    <a class="pull-right" href="#">Reply</a>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Nguyễn Tuấn Huy</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="patientModal2" tabindex="-1" role="dialog" aria-labelledby="patientModal2" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Trần Thanh Thúy's Blood Pressure Record</h4>
                    </div>
                    <div class="modal-body">
                        <img src="/img/sample/linechart2.jpg"  width="100%">
                        <div class="action-box margin-t">
                            <textarea placeholder="Create your topic..." rows="3" class="form-control"></textarea>
                            <div class="action-list">
                                <div class="action-item">
                                    <a title="" class="fa fa-picture-o" rel="tooltip" data-original-title="Post an Image"><i></i></a>
                                    <a title="" class="fa fa-video-camera" rel="tooltip" data-original-title="Upload a Video"><i></i></a>
                <!--                    <a title="" class="fa fa-lightbulb-o" rel="tooltip" data-original-title="Post an Idea"><i></i></a>
                                    <a title="" class="fa fa-question-circle" rel="tooltip" data-original-title="Ask a Question"><i></i></a>-->
                                </div>	
                                <div class="pull-right">
                                    <a class="btn btn-primary btn-sm">Post</a>
                                </div>
                            </div>
                        </div>
                        <div class="margin-b">
                            <div class="feed-item">
                                <div class="feed-icon">
                                    <!--<i class="fa fa-lightbulb-o"></i>-->
                                    <a href="#"><img src="/img/sample/doctor.jpg" class="img-thumbnail"></a>
                                </div>
                                <div class="feed-subject">
                                    <p>
                                        <a href="#">Dr Trần Công Minh</a>
                                        <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                    </p>
                                </div>
                                <div class="feed-content">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                </div>
                                <div class="feed-actions">
                    <!--                <a class="pull-left" rel="tooltip"><i class="fa fa-thumbs-up"></i> 123</a> 
                                    <a class="pull-left" rel="tooltip"><i class="fa fa-comment-o"></i> 29</a>-->
                                    <a class="pull-right" href="#">Reply</a>
                                </div>
                                <div class="feed-item">
                                    <div class="feed-icon">
                                        <a href="#"><img src="/img/sample/patient.jpg" class="img-thumbnail"></a>
                                    </div>
                                    <div class="feed-subject">
                                        <p>
                                            <a href="#">Nguyễn Tuấn Huy</a>
                                            <span class="pull-right text-muted"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                        </p>
                                    </div>
                                    <div class="feed-content">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<!--    <div class="col-sm-7">
        
    </div>-->
</div>


<?/*<div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                    <i class="fa fa-bars fa-lg"></i>
                </span>
                Summary
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <div class="panel mini-box">
                        <span class="box-icon bg-primary">
                            <i class="fa fa-users"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">30</p>
                                <p class="text-muted">Total Patients</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel mini-box">
                        <span class="box-icon bg-primary">
                            <i class="fa fa-users"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">5</p>
                                <p class="text-muted">Pending Patient Requests</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel mini-box">
                        <span class="box-icon bg-danger">
                            <i class="fa fa-warning"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">10/5</p>
                            <p class="text-muted">Warnings/Patients</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel mini-box">
                        <span class="box-icon bg-info">
                            <i class="fa fa-list"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">30</p>
                            <p class="text-muted">Record Notes</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel mini-box">
                        <span class="box-icon bg-warning">
                            <i class="fa fa-comments"></i>
                        </span>
                        <div class="box-info">
                            <p class="size-h2">20</p>
                            <p class="text-muted">Discussion Topics</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>*/?>