<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/doctor.jpg"></a>
            <a href=""><h3>Dr Trần Công Minh</h3></a>
            <ul class="nav nav-info">
                <li>Male, 50</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li><a href="/doctor/index">Dashboard</a></li>
            <li class="active"><a href="/doctor/patient">Patient</a></li>
            <!--<li><a href="/doctor/community">Community</a></li>-->
        </ul>
    </div>
</div>
<div class="container">
    <div class="col-sm-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                     <i class="fa fa-group fa-lg"></i>
                </span>
                Patient List
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover user-list">
                        <thead>
                            <tr>
                                <th>Patient</th>
                                <!--<th>Relationship</th>-->
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/son.jpg"></a>
                                    <a href="">Nguyễn Tuấn Huy</a>
                                </td>
                                <!--<td>Son</td>-->
                                <td><span class="label label-info">CONFIRM</span></td>
                                <td><a href="">Remove</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-5">  
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                     <i class="fa fa-group fa-lg"></i>
                </span>
                Patient Request
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover user-list">
                        <thead>
                            <tr>
                                <th>Patient</th>
                                <!--<th>Status</th>-->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/user3.jpg"></a>
                                    <a href="">Nguyễn Minh Duy</a>
                                </td>
                                <!--<td><span class="label label-default">PENDING</span></td>-->
                                <td><a href="">Accept</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>