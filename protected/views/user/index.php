<div class="row-fluid info-block">
    <div class="container">
        <div class="col-sm-6 infol">
            <a href=""><img class="img-circle img-thumbnail" src="/img/sample/son.jpg"></a>
            <a href=""><h3>Nguyễn Tuấn Minh</h3></a>
            <ul class="nav nav-info">
                <li>Male, 35</li>
                <li>Hồ Chí Minh</li>
            </ul>
        </div>
        <div class="col-sm-6 ">
            <a href="/user/profile" class="btn btn-primary pull-right">User Profile</a>
        </div>
    </div>
</div>
<div class="row-fluid tab-block">
    <div class="container">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/user/index">Patient</a></li>
            <li><a href="/user/community">Community</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="col-sm-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="box-icon">
                     <i class="fa fa-group fa-lg"></i>
                </span>
                Patient List
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover user-list">
                        <thead>
                            <tr>
                                <th class="sorting">Patient</th>
                                <th>Relationship</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="text-left">
                                    <a href=""><img class="avatar-small img-thumbnail" src="/img/sample/patient.jpg"></a>
                                    <a href="">Nguyễn Tuấn Huy</a>
                                </td>
                                <td>Father</td>
                                <td><span class="label label-default">PENDING</span></td>
                                <td>
                                    <a href="">Accept</a> | <a href="">Deny</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
<!--                <div class="text-center">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Guardian</a>
                </div>-->
            </div>
        </div>
    </div>
</div>