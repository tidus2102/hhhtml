<!DOCTYPE html>
<html lang="en">
<head>
    <title>HelloHealth</title>
    
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/fonts/fontello.css" />
    <!--<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css" />-->
    <!--<link rel="stylesheet" type="text/css" href="/css/perfect-scrollbar.min.css" />-->
    
    <!--<link rel="stylesheet" type="text/css" href="/css/hh.css" />-->
    <!--<link rel="stylesheet/less" type="text/css" href="/css/hh.less" />-->
    
    <link rel="stylesheet" type="text/css" href="/css/slidepushmenus.css" />
    <link rel="stylesheet" type="text/css" href="/lib/bxslider/jquery.bxslider.css" />
    
    <link rel="stylesheet" type="text/css" href="/css/company.css" />
    <link rel="stylesheet" type="text/css" href="/css/company-reponsive.css" />

    <!--<link rel="shortcut icon" href="/img/favicon0.ico" />-->	
    
    <?php 
        $cs = Yii::app()->clientScript;
        $cs->scriptMap=array(
            #'jquery.js' => '/js/jquery.js',
            'jquery.js' => '/js/jquery.min.js',
            'jquery.min.js' => '/js/jquery.min.js',#use for production mode
        );
        $cs->registerCoreScript('jquery') ?>
    <?#php $cs->registerScriptFile("/js/bootstrap.min.js") ?>
    
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="/js/jquery.gmap.min.js"></script>
    <script type="text/javascript" src="/lib/bxslider/jquery.bxslider.min.js"></script>
<!--    <script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
    <script type="text/javascript" src="/js/less-1.7.0.min.js"></script>-->
</head>

<body class="cbp-spmenu-push" id="body">
    <section class="section2" id="home">
<!--        <div class="mini-navbar" id="top">
            <div class="container">
                <i class="fa fa-mobile"></i> <a href="tel:+8419006135"><b>1900.6.135</b></a>
                |
                <i class="fa fa-envelope"></i> <a href="mailto:info@hellohealth.vn">info@hellohealth.vn</a>
                <div class="pull-right">
                    <div class="language">
                        <span><a href="#">Tiếng Việt</a></span>
                        |
                        <span class="active"><a href="#">English</a></span>
                    </div>
                    <a class="" href="/login"><i class="fa fa-sign-in"></i> SIGN IN</a>
                </div>
            </div>
        </div>-->
        <header class="">
            <div class="container">
                <div class="">
                    <!--<button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle" id="showRightPush">-->
                    <button class="navbar-toggle" id="showRightPush">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand"><img src="/img/logocolor_large.png"></a>
                    <!--<a href="/login" class="btn btn-primary pull-right">SIGN IN</a>-->
                </div>
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
                    <h3>HelloHealth</h3>
                    <!-- <a href="/">Home</a>
                    <a href="/#pricing">Pricing</a>
                    <a href="/doctor">Doctor</a>
                    <a href="/article">Health Info</a> -->
                    <a href="#about" class="scroll">About</a>
                    <a href="#service" class="scroll">Services</a>
                    <a href="#pricing" class="scroll">Plan</a>
                    <a href="#doctor" class="scroll">Doctor</a>
                    <a href="#health" class="scroll">News</a>
                    <a href="#contact" class="scroll">Contact</a>
                </nav>
                <!--<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">-->
                <nav role="navigation" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li><a href="/">Home</a></li>
                        <li><a href="/#pricing">Pricing</a></li>
                        <li><a href="/doctor" >Doctor</a></li>
                        <li><a href="/article">Health Info</a></li> -->
<!--                        <li><a href="#about" class="scroll">About</a></li>
                         <li><a href="#product" class="scroll">Product</a></li> 
                        <li><a href="#service" class="scroll">Services</a></li>
                        <li><a href="#pricing" class="scroll">Plan</a></li>
                        <li><a href="#doctor" class="scroll">Doctor</a></li>
                        <li><a href="#health" class="scroll">News</a></li>
                        <li><a href="#contact" class="scroll">Contact</a></li>-->
                        <li><a href="/login" >Sign In</a></li>
                        <li><a href="/signin" class="btn">Sign Up</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        
        <ul class="bxslider homeslider" id="homeslider">
            <li class="bxslide homeslide homeslide1">
                <div class="container">
                    <div class="text-center col-sm-offset-1 col-sm-10">
                        <h1>Smarter Healthcare</h1>
                        <h2>Take care of yourself & your family anytime, anywhere with HelloHealth mobile healthcare solution.</h2>
<!--                        <h2>Take care of your health and your family. Anytime. Anywhere.</h2>-->
                        <div class="btn-action">
                            <a class="btn btn-transparent btn-lg scroll" href="#about">EXPERIENCE MORE</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="bxslide homeslide homeslide2">
                <div class="container">
                    <div class="text-center col-sm-offset-1 col-sm-10">
                        <h1>Smarter Healthcare</h1>
                        <h2>Take care of yourself & your family anytime, anywhere with HelloHealth mobile healthcare solution.</h2>
<!--                        <h2>Take care of your health and your family. Anytime. Anywhere.</h2>-->
                        <div class="btn-action">
                            <a class="btn btn-transparent btn-lg scroll" href="#about">EXPERIENCE MORE</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="bxslide homeslide homeslide3">
                <div class="container">
                    <div class="text-center col-sm-offset-1 col-sm-10">
                        <h1>Smarter Healthcare</h1>
                        <h2>Take care of yourself & your family anytime, anywhere with HelloHealth mobile healthcare solution.</h2>
<!--                        <h2>Take care of your health and your family. Anytime. Anywhere.</h2>-->
                        <div class="btn-action">
                            <a class="btn btn-transparent btn-lg scroll" href="#about">EXPERIENCE MORE</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    
    <?php echo $content; ?>
    
    <footer>
        <i class="gi-gradient-line"></i>
        <div class="container text-center">
            <div class="menu">
                <a href="#">Terms</a>
                <a href="#">Support</a>
                <a href="#">Blog</a>
            </div>
            <p class="copyright">
                &copy; HelloHealth <?= date('Y') ?>. All rights reserved.
            </p>
            <div class="social">
                <a href="https://www.facebook.com/hellohealth.vn" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a href="https://plus.google.com/113279156803633458065" target="_blank" title="Google+"><i class="fa fa-google-plus"></i></a>
                <a href="https://twitter.com/hellohealthvn" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                <a href="https://www.youtube.com/channel/UCuaoCZMW-hSTus1AEvO9OiA" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </footer>
    
    <ul class="fix-sidebar" id="gototop">
        <li>
            <a title="Go to Top" href="#home" class="scroll">
                <i class="fa fa-angle-up fa-lg"></i>
            </a>
        </li>
<!--        <li class="facebook">
            <a href="#home" class="scroll">Home</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Product</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Service</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Home</a>
        </li>-->
    </ul>
    
<script>
    
    $(document).ready(function() {
        $("[rel='tooltip']").tooltip(); 
        $("[rel='popover']").popover({
            trigger: 'hover'
        }); 
    });
//    $(function () {
//        var DP = $('.datepicker').datepicker({
//            format: 'dd-mm-yyyy'
//        }).on('changeDate', function(ev) {
//            DP.datepicker('hide');
//        });
//    });  
    
//    $(document).ready(
//        function() {
//            $('.items').perfectScrollbar({wheelSpeed: 40});
//        }
//    );

//    $(document).on('click', '.changeLanguage', function(event){
//        event.preventDefault();
//        
//        var lang = $(this).attr('lang');
//        
//        var url = '/site/changeLanguage';
//        $.post(url, {lang: lang}, function(data) {
//            if(data.success == true){
//                location.reload();
//            }
//        },'json');
//    });
</script>

</body>
</html> 


<?/*<div data-ride="carousel" data-interval='false' class="carousel fade in" id="headercarousel">
<!--        <div data-ride="carousel" class="carousel slide" id="headercarousel">-->
            <div class="carousel-inner ">
                <div class="slide1 item active" data-interval="4000">
                    <div class="container">
                        <div class="text-center header-content">
                            <h1>HELLOHEALTH</h1>
                            <h2>Take care of your health and your family. Anytime. Anywhere.</h2>
                            <div class="btn-action">
                                <!--<a class="btn btn-primary btn-lg scroll" href="#pricing">START NOW</a>-->
                                <a class="btn btn-transparent btn-lg scroll" href="#top">EXPERIENCE MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide2 item" data-interval="4000">
                    <div class="container">
                        <div class="text-center header-content ">
                            
                        </div>
                    </div>
                </div>
                <div class="slide3 item" data-interval="4000">
                    <div class="container">
                        <div class="text-center header-content col-sm-6">
                            <h1>HELLOHEALTH</h1>
                            <h2>Take care of your health and your family. Anytime. Anywhere.</h2>
                            <div class="btn-action">
                                <!--<a class="btn btn-primary btn-lg scroll" href="#pricing">START NOW</a>-->
                                <a class="btn btn-transparent btn-lg scroll" href="#top">EXPERIENCE MORE</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <img src="/img/company/user2.jpg" class="img-circle">
                        </div>
                    </div>
                </div>
            </div>
    <!--                <ol class="carousel-indicators">
                <li class="active" data-slide-to="0" data-target="#maincarousel"></li>
                <li data-slide-to="1" data-target="#maincarousel" class=""></li>
                <li data-slide-to="2" data-target="#maincarousel" class=""></li>
            </ol>-->
            <a data-slide="prev" href="#headercarousel" role="button" class="left carousel-control" id="left-carousel-control">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a data-slide="next" href="#headercarousel" role="button" class="right carousel-control" id="right-carousel-control">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>*/?>