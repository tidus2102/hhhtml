<?/*<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>*/?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>HelloHealth</title>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <!--<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css" />-->
    <!--<link rel="stylesheet" type="text/css" href="/css/perfect-scrollbar.min.css" />-->

    <!--<link rel="stylesheet" type="text/css" href="/css/hh.css" />-->
    <!--<link rel="stylesheet/less" type="text/css" href="/css/hh.less" />-->

    <link rel="stylesheet" type="text/css" href="/css/company.css" />
    <link rel="stylesheet" type="text/css" href="/css/company-reponsive.css" />

    <link rel="stylesheet" type="text/css" href="/css/slidepushmenus.css" />

	<!--<link rel="shortcut icon" href="/img/favicon0.ico" />-->

    <?php
        $cs = Yii::app()->clientScript;
        $cs->scriptMap=array(
            #'jquery.js' => '/js/jquery.js',
            'jquery.js' => '/js/jquery.min.js',
            'jquery.min.js' => '/js/jquery.min.js',#use for production mode
        );
        $cs->registerCoreScript('jquery') ?>
    <?#php $cs->registerScriptFile("/js/bootstrap.min.js") ?>
</head>

<body class="cbp-spmenu-push" id="body">
    <div class="mini-navbar" id="top">
        <div class="container">
            <i class="fa fa-mobile"></i> <a href="tel:+8419006135"><b>1900.6.135</b></a>
            |
            <i class="fa fa-envelope"></i> <a href="mailto:info@hellohealth.vn">info@hellohealth.vn</a>
            <div class="pull-right">
                <div class="language">
                    <span><a href="#">Tiếng Việt</a></span>
                    |
                    <span class="active"><a href="#">English</a></span>
                </div>
                <a class="" href="/login"><i class="fa fa-sign-in"></i> SIGN IN</a>
            </div>
        </div>
    </div>

    <!--<header class="navbar-fixed-top">-->
    <header class="" id="fixtopmenu">
        <div class="container">
            <div class="">
                <!--<button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle" id="showRightPush">-->
                <button class="navbar-toggle" id="showRightPush">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand"><img src="/img/logocolor.png"></a>
                <!--<a href="/login" class="btn btn-primary pull-right">SIGN IN</a>-->
            </div>
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
                <h3>HelloHealth</h3>
                <a href="/">Home</a>
                <!--<a href="/#services">Services</a>-->
                <a href="/#pricing">Pricing</a>
                <a href="/doctor">Doctor</a>
                <a href="/article">Health Info</a>
                <a href="/#contact">Contact</a>
            </nav>
            <!--<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">-->
            <nav role="navigation" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/">Home</a></li>
<!--                    <li><a href="/#services">Services</a></li>-->
                    <li><a href="/#pricing" >Pricing</a></li>
                    <li><a href="/doctor" >Doctor</a></li>
                    <li><a href="/article">Health Info</a></li>
                    <li><a href="/#contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <?php echo $content; ?>

    <footer>
        <i class="gi-gradient-line"></i>
        <div class="container text-center">
            <div class="menu">
                <a href="#">Terms</a>
                <a href="#">Support</a>
                <a href="#">Blog</a>
            </div>
            <p class="copyright">
                &copy; HelloHealth <?= date('Y') ?>. All rights reserved.
            </p>
            <div class="social">
                <a href="https://www.facebook.com/hellohealth.vn" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a href="https://plus.google.com/113279156803633458065" target="_blank" title="Google+"><i class="fa fa-google-plus"></i></a>
                <a href="https://twitter.com/hellohealthvn" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                <a href="https://www.youtube.com/channel/UCuaoCZMW-hSTus1AEvO9OiA" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </footer>


    <ul class="fix-sidebar" id="gototop">
        <li>
            <a title="Go to Top" href="#top" class="scroll">
                <i class="fa fa-angle-up fa-lg"></i>
            </a>
        </li>
<!--        <li class="facebook">
            <a href="#home" class="scroll">Home</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Product</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Service</a>
        </li>
        <li class="facebook">
            <a href="#home" class="scroll">Home</a>
        </li>-->
    </ul>

<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="/js/jQuery.headroom.min.js"></script>
<script type="text/javascript" src="/js/headroom.min.js"></script>-->
<!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="/js/jquery.gmap.min.js"></script>-->
<!--<script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>-->
<!--<script type="text/javascript" src="/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
<script type="text/javascript" src="/js/less-1.7.0.min.js"></script>-->

<script>
//    $(document).ready(
//        function() {
//            $('.smoothScroll').smoothScroll();
//        }
//    );
//    $('#fixtopmenu').headroom();

    $(function ($) {
        $("[rel='tooltip']").tooltip();
        $("[rel='popover']").popover({
            trigger: 'hover'
        });
//        $('textarea').autosize();

//        $('.perfectScrollbar').perfectScrollbar({
//            wheelSpeed: 40
//        });
    });

    //Google map
    /*jQuery('#map').gMap({
        address: "88a Trần Huy Liệu, phường 15, Thành phố Hồ Chí Minh, quận Phú Nhuận, Hồ Chí Minh, Vietnam",
        zoom: 16,
        controls: {
//         panControl: true,
         zoomControl: true,
         mapTypeControl: true,
         scaleControl: true,
//         streetViewControl: true,
         overviewMapControl: true
     },
        markers: [{
            address: "88a Trần Huy Liệu, phường 15, Thành phố Hồ Chí Minh, quận Phú Nhuận, Hồ Chí Minh, Vietnam",
            html: "<div class='text-center'><img src='/img/logocolor.png'><p>Smarter Healthcare</p><p><span class='icon glyphicon glyphicon-map-marker'></span> 88A Trần Huy Liệu, P.15, Q.Phú Nhuận, Tp HCM</p><p><span class='icon glyphicon glyphicon-phone'></span> 1900.6.135</p><p><span class='icon glyphicon glyphicon-envelope'></span> <a href='mailto:info@hellohealth.vn'>info@hellohealth.vn</a></p></div>",
            popup: true
        }]
    });*/

//    $(function () {
//        var DP = $('.datepicker').datepicker({
//            format: 'dd-mm-yyyy'
//        }).on('changeDate', function(ev) {
//            DP.datepicker('hide');
//        });
//    });

//    $(document).ready(
//        function() {
//            $('.items').perfectScrollbar({wheelSpeed: 40});
//        }
//    );



//    $(document).on('click', '.changeLanguage', function(event){
//        event.preventDefault();
//
//        var lang = $(this).attr('lang');
//
//        var url = '/site/changeLanguage';
//        $.post(url, {lang: lang}, function(data) {
//            if(data.success == true){
//                location.reload();
//            }
//        },'json');
//    });
</script>

</body>
</html>