<?/*<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>*/?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css" />
    <!--<link rel="stylesheet" type="text/css" href="/css/perfect-scrollbar.min.css" />-->
    
    <link rel="stylesheet" type="text/css" href="/css/hh.css" />
    <!--<link rel="stylesheet/less" type="text/css" href="/css/hh.less" />-->

	<!--<link rel="shortcut icon" href="/img/favicon0.ico" />-->	
    
    <?php 
        $cs = Yii::app()->clientScript;
        $cs->scriptMap=array(
            #'jquery.js' => '/js/jquery.js',
            'jquery.js' => '/js/jquery.min.js',
            'jquery.min.js' => '/js/jquery.min.js',#use for production mode
        );
        $cs->registerCoreScript('jquery') ?>
    <?#php $cs->registerScriptFile("/js/bootstrap.min.js") ?>
    
    <title>HelloHealth HTML</title>
</head>

<body>
    <header>
<?/*<div class="pull-right">
            <a href="#" class="changeLanguage" lang="vi" title="Vietnamese"><img class="" src="/img/vflag.png"></a>
            <a href="#" class="changeLanguage" lang="en" title="English"><img class="" src="/img/uflag.png"></a>
        </div>*/?>
        <div class="container">
            <div class="navbar-header">
                <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand"><img src="/img/logowhite.png"></a>
            </div>
            <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/">Home</a></li>
                    <!--<li><a href="#">Community</a></li>-->
                    <!--<li><a href="/signup" class="btn">SIGN UP</a></li>-->
                </ul>
            </nav>
        </div>
    </header>
    
    <main class="body-content">
        <?php echo $content; ?>
    </main>
    
    <footer>
        <div class="container">
            <div class="row-fluid">
                <div class="menu">
                    <a href="/">HelloHealth</a>
                    <a href="/about">About Us</a>
                    <a href="/buy">Pricing</a>
                    <a href="/terms">Terms</a>
                    <a href="/support">Support</a>
                </div>
                <div class="social pull-right">
                    <a href="" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="" title="Google+"><i class="fa fa-google-plus"></i></a>
                    <a href="" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="" title="Youtube"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <p class="copyright">
                &copy; HelloHealth <?= date('Y') ?>. All rights reserved.
            </p>
        </div>
    </footer>

<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
<!--<script type="text/javascript" src="/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
<script type="text/javascript" src="/js/less-1.7.0.min.js"></script>-->

<script type="text/javascript">
    $(function () {
        var DP = $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        }).on('changeDate', function(ev) {
            DP.datepicker('hide');
        });
    });  
    
//    $(document).ready(
//        function() {
//            $('.items').perfectScrollbar({wheelSpeed: 40});
//        }
//    );

//    $(function ($) { 
//        $("[rel='tooltip']").tooltip(); 
////        $("[data-toggle='popover']").popover(); 
//        $('textarea').autosize();
//        
//        $('.perfectScrollbar').perfectScrollbar({
//            wheelSpeed: 40
//        });
//    });
    
//    $(document).on('click', '.changeLanguage', function(event){
//        event.preventDefault();
//        
//        var lang = $(this).attr('lang');
//        
//        var url = '/site/changeLanguage';
//        $.post(url, {lang: lang}, function(data) {
//            if(data.success == true){
//                location.reload();
//            }
//        },'json');
//    });
</script>

</body>
</html>
