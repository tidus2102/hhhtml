<?/*<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>*/?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="/css/perfect-scrollbar.min.css" />
    
    <link rel="stylesheet" type="text/css" href="/css/hh.css" />
    <!--<link rel="stylesheet/less" type="text/css" href="/css/hh.less" />-->

	<!--<link rel="shortcut icon" href="/img/favicon0.ico" />-->	
    
    <?php 
        $cs = Yii::app()->clientScript;
        $cs->scriptMap=array(
            #'jquery.js' => '/js/jquery.js',
            'jquery.js' => '/js/jquery.min.js',
            'jquery.min.js' => '/js/jquery.min.js',#use for production mode
        );
        $cs->registerCoreScript('jquery') ?>
    <?#php $cs->registerScriptFile("/js/bootstrap.min.js") ?>
    
    <title>HelloHealth HTML</title>
    
    <?# Yii::app()->clientScript->registerScript('create_tooltip', '$("[rel=tooltip]").tooltip();', CClientScript::POS_READY); ?>
    <?#php Yii::app()->clientScript->registerScript('create_popover', '$("[rel=popover]").popover();', CClientScript::POS_READY); ?>
</head>

<body>
    <header>
<?/*<div class="pull-right">
            <a href="#" class="changeLanguage" lang="vi" title="Vietnamese"><img class="" src="/img/vflag.png"></a>
            <a href="#" class="changeLanguage" lang="en" title="English"><img class="" src="/img/uflag.png"></a>
        </div>*/?>
        <div class="container">
            <div class="navbar-header">
                <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="" class="navbar-brand"><img src="/img/logowhite.png"></a>
            </div>
            <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="noti-icon dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle" id="warning-list">
                            <i class="fa fa-warning fa-lg"></i>
                            <span class="badge">3</span>
                        </a>
                        <div aria-labelledby="warning-list" class="dropdown-menu noti-box">
                            <div class="noti-header">Warnings</div>
                            <div class="noti-body perfectScrollbar">
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <i class="fa fa-warning bg-warning"></i>
                                        </span>
                                        <span class="noti-content">
                                            <p>Your blood pressure record is Hypertension</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <i class="fa fa-warning bg-danger"></i>
                                        </span>
                                        <span class="noti-content">
                                            <p>Your blood pressure record is Hypertension Emergency</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item noti-viewed">
                                    <a href="">
                                        <span class="noti-img">
                                            <i class="fa fa-warning bg-warning"></i>
                                        </span>
                                        <span class="noti-content">
                                            <p>Your blood pressure record is Hypertension</p>
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="noti-footer text-center"><a href="/patient/notification">View all warnings</a></div>
                        </div>
                    </li>
                    <li class="noti-icon dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" id="noti-list" href="#">
                            <i class="fa fa-globe fa-lg"></i>
                            <span class="badge">5</span>
                        </a>
                        <div aria-labelledby="noti-list" class="dropdown-menu noti-box">
                            <div class="noti-header">Notifications</div>
                            <div class="noti-body perfectScrollbar">
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <img src="/img/sample/doctor.jpg">
                                        </span>
                                        <span class="noti-content">
                                            <p><b>Dr Trần Công Minh</b> added a note on your record</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item noti-viewed">
                                    <a href="">
                                        <span class="noti-img">
                                            <img src="/img/sample/doctor.jpg">
                                        </span>
                                        <span class="noti-content">
                                            <p><b>Dr Trần Công Minh</b> added a note on your record</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <img src="/img/sample/doctor.jpg">
                                        </span>
                                        <span class="noti-content">
                                            <p><b>Dr Trần Công Minh</b> added a note on your record</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <img src="/img/sample/doctor.jpg">
                                        </span>
                                        <span class="noti-content">
                                            <p><b>Dr Trần Công Minh</b> added a note on your record</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                                <div class="noti-item">
                                    <a href="">
                                        <span class="noti-img">
                                            <img src="/img/sample/doctor.jpg">
                                        </span>
                                        <span class="noti-content">
                                            <p><b>Dr Trần Công Minh</b> added a note on your record</p>
                                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing edivt...</span>-->
                                            <span class="noti-time">20 minutes ago</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="noti-footer text-center"><a href="/patient/notification">View all notifications</a></div>
                        </div>
                    </li>
                    <!--<li><a href="#">Community</a></li>-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" id="user-menu" href="#">Nguyễn Tuấn Huy <b class="caret"></b></a>
                        <ul aria-labelledby="user-menu" class="dropdown-menu">
                            <li><a href="/patient/">Dashboard</a></li>
                            <li><a href="/patient/account">Account</a></li>
                            <li><a data-toggle="modal" data-target="#inviteModal">Invite</a></li>
                            <li class="divider"></li>
                            <li><a href="/">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    
    <main class="body-content">
        <?php echo $content; ?>
    </main>
    
    <footer>
        <div class="container">
            <div class="row-fluid">
                <div class="menu">
                    <a href="/">HelloHealth</a>
                    <a href="/about">About Us</a>
                    <a href="/buy">Pricing</a>
                    <a href="/terms">Terms</a>
                    <a href="/support">Support</a>
                </div>
                <div class="social pull-right">
                    <a href="https://www.facebook.com/hellohealth.vn" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/113279156803633458065" target="_blank" title="Google+"><i class="fa fa-google-plus"></i></a>
                    <a href="https://twitter.com/hellohealthvn" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.youtube.com/channel/UCuaoCZMW-hSTus1AEvO9OiA" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <p class="copyright">
                &copy; HelloHealth <?= date('Y') ?>. All rights reserved.
            </p>
        </div>
    </footer>

<div class="modal fade" id="inviteModal" tabindex="-1" role="dialog" aria-labelledby="inviteModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Send Invitation</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <!--<label class="col-sm-2 control-label">Name</label>-->
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Name">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="Email">
                        </div>
                    </div>
<!--                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                    </div>-->
                    <div class="form-group">
                        <!--<label class="col-sm-2 control-label">Message</label>-->
                        <div class="col-sm-12">
                            <textarea row="3" class="form-control" placeholder="Message..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-9 col-sm-3">
                            <button type="submit" class="btn btn-primary">Send Invitation</button>
                        </div>
                    </div>
                </form>
            </div>
<!--            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>

<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
<!--<script type="text/javascript" src="/js/less-1.7.0.min.js"></script>-->

<script type="text/javascript">
    $(function () {
        var DP = $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        }).on('changeDate', function(ev) {
            DP.datepicker('hide');
        });
    });  
    
//    $(document).ready(
//        function() {
//            $('.items').perfectScrollbar({wheelSpeed: 40});
//        }
//    );

    $(function ($) { 
        $("[rel='tooltip']").tooltip(); 
//        $("[data-toggle='popover']").popover(); 
        $('textarea').autosize();
        
        $('.perfectScrollbar').perfectScrollbar({
            wheelSpeed: 40
        });
    });
    
//    $(document).on('click', '.changeLanguage', function(event){
//        event.preventDefault();
//        
//        var lang = $(this).attr('lang');
//        
//        var url = '/site/changeLanguage';
//        $.post(url, {lang: lang}, function(data) {
//            if(data.success == true){
//                location.reload();
//            }
//        },'json');
//    });
</script>

</body>
</html>
